﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SA;
namespace Bubble
{
    public class BubbleShoot_3 : MonoBehaviour
    {

        public static BubbleShoot_3 m_instance;
        public static BubbleShoot_3 Instance()
        {
            return m_instance;
        }
        public BubbleShoot_3()
        {
            m_instance = this;
        }

        //尋找物件池
        [Header("Find The Object Pool")]
        public GameObject go_pool;
        public BubblePool pool;
        public GameObject go_ballPool;
        public BigBallPool ballPool;
        public GameObject go_sameSpeedPool;
        public SameSpeedBubblePool sameSpeedPool;
        public GameObject go_bigBallOnePattern;
        public BigBallPool_OnePattern1 bigBallOnePattern;
        public LaserColliderPool laserColliderPool;

        [Header("Find AI_data")]
        public AI_data data;
        public EnemyTarget enemyTarget;
        
        // Start is called before the first frame update
        void Start()
        {
            go_pool = GameObject.FindGameObjectWithTag("ObjectPool");
            pool = go_pool.GetComponent<BubblePool>();
            go_ballPool =  GameObject.FindGameObjectWithTag("ObjectPool");
            ballPool = go_ballPool.GetComponent<BigBallPool>();
            go_sameSpeedPool = GameObject.FindGameObjectWithTag("ObjectPool");
            sameSpeedPool = go_sameSpeedPool.GetComponent<SameSpeedBubblePool>();
            go_bigBallOnePattern = GameObject.FindGameObjectWithTag("ObjectPool");
            bigBallOnePattern = go_bigBallOnePattern.GetComponent<BigBallPool_OnePattern1>();
            laserColliderPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<LaserColliderPool>();

            enemyTarget = GetComponent<EnemyTarget>();
            //if (enemyTarget.b_IsSpiderRobot )
            //{
            //    emitterHolder = GameObject.FindGameObjectWithTag("MobEmitter");
            //}
            //else if (enemyTarget.b_IsLastBoss)
            //{
            //    emitterHolder = GameObject.FindGameObjectWithTag("LB_EmitterHolder");
            //}

            data = this.GetComponent<AI_data>();
            
            
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            //BubbleShooting();
        }

        [Header("Rotation Settings")]
        public GameObject emitterHolder;
        public float rotationSpeed = 1.0f;
        public float rotationAngleX = 30.0f;
        public float rotationAngleY = 0f;
        public float rotationAngleZ = 0f;

        
        /// <summary>
        /// 讓泡泡發射口乘載的東西旋轉，創造出泡泡旋轉的路徑
        /// </summary>
        public void BubbleMakerRotate()//讓泡泡發射口乘載的東西旋轉，創造出泡泡旋轉的路徑
        {
            if (data.b_LBEmitterRotate == false) { return; }
            emitterHolder.transform.Rotate(rotationAngleX, rotationAngleY, rotationAngleZ);
        }

        /// <summary>
        /// 讓泡泡發射口乘載的東西旋轉，創造出泡泡旋轉的路徑，直接丟參數用，要在update裡才會一直轉
        /// </summary>
        public void BubbleMakerRotate(float rotationAngleX, float rotationAngleY, float rotationAngleZ)
        {
            if (data.b_LBEmitterRotate == false) { return; }
            emitterHolder.transform.Rotate(rotationAngleX, rotationAngleY, rotationAngleZ);
        }
        /// <summary>
        /// 讓泡泡發射口乘載的東西旋轉，創造出泡泡旋轉的路徑，直接丟參數用，可以送他一個GameObject，要在update裡才會一直轉
        /// </summary>
        public void BubbleMakerRotate(GameObject emitter, float rotationAngleX, float rotationAngleY, float rotationAngleZ)
        {
            if (data.b_LBEmitterRotate == false) { return; }
            emitter.transform.Rotate(rotationAngleX, rotationAngleY, rotationAngleZ);
        }
        public void BubbleMakerRotateForever(float rotationAngleX, float rotationAngleY, float rotationAngleZ)
        {
            emitterHolder.transform.Rotate(rotationAngleX, rotationAngleY, rotationAngleZ);
        }

        private void OnDrawGizmos()//泡泡發射方向
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(this.transform.position, this.transform.position + this.transform.forward);
        }


        [Header("Bubble PreFabs")]
        public GameObject[] bubbles;
        public int bubblesIndex = 0;
        private int randomBubblesIndex;
        public GameObject bigBall;

        [Header("Bubble ShootingTime Settings")]
        public float firstShootingCountDown;//第一次射出的時間，不重要，不給初值的話見到玩家馬上發射
        public float resetShootingTime;//重新指派給下一發子彈的射出時間，越小間隔越短，影響到射速
        public float totalWaveShootingTime;//每一波射出時間總長
        public float rechargeTime;//每一波射完之後的等待時間

        public Transform[] bubbleEmitter;//泡泡的發射口

        [Header("Bubble Timer")]
        [SerializeField] private float shootingTimeCountDown;//要準備發射子彈的倒數時間，監測用
        [SerializeField] private float totalWaveShootingTimeCountDown;//每波射出時間總長的倒數，監測用，這個要給初值才會進入函式
        [SerializeField] private float rechargeTimeCountDown;//等待時間的倒數，要給初值，會影響第一次射完之後的間格，監測用




        #region 基礎產生泡泡
        //產生泡泡的三種方式
        public void OneColorBubble()//單色泡泡
        {
            for (int i = 0; i < bubbleEmitter.Length; i++)
            {
                GameObject bubbleClone = Instantiate(bubbles[bubblesIndex], bubbleEmitter[i].transform.position, bubbleEmitter[i].transform.rotation) as GameObject;
            }

        }
        public void RandomColorBubbles()//隨機顏色泡泡
        {
            for (int i = 0; i < bubbleEmitter.Length; i++)
            {
                randomBubblesIndex = Random.Range(0, bubbles.Length);
                GameObject bubbleClone = Instantiate(bubbles[randomBubblesIndex], bubbleEmitter[i].transform.position, bubbleEmitter[i].transform.rotation) as GameObject;
            }
        }
        public void SortedColorBubbles()//順序產生彩色泡泡
        {

            if (bubblesIndex < bubbles.Length)
            {
                for (int i = 0; i < bubbleEmitter.Length; i++)
                {
                    GameObject bulletClone = Instantiate(bubbles[bubblesIndex], bubbleEmitter[i].transform.position, bubbleEmitter[i].transform.rotation) as GameObject;
                }
                bubblesIndex++;
            }
            else
            {
                bubblesIndex = 0;
            }
        }
        #endregion

        #region 物件池射擊法
        private void MakeBubbleWithPools()
        {
            for (int i = 0; i < bubbleEmitter.Length; i++)//多個發射口一齊發射
            {
                pool.ReUse(bubbleEmitter[i].transform.position, bubbleEmitter[i].transform.rotation);//使用BubblePool的方法ReUse，把SetActive(false)的物件拿出來變true
            }
        }

        /// <summary>
        /// //多個發射口一齊發射公開用，只射一波，存放Transform的
        /// </summary>
        public void MakeBubbleWithPools(Transform[] emitters)//多個發射口一齊發射公開用，只射一波
        {
            for (int i = 0; i < emitters.Length; i++)//多個發射口一齊發射
            {
                pool.ReUse(emitters[i].transform.position, emitters[i].transform.rotation);//使用BubblePool的方法ReUse，把SetActive(false)的物件拿出來變true
            }
        }
        
        /// <summary>
        /// //多個發射口一齊發射公開用，只射一波，存放物件的自動取他的transform
        /// </summary>
        public void MakeBubbleWithPools(GameObject[] emitters)
        {
            for (int i = 0; i < emitters.Length; i++)//多個發射口一齊發射
            {
                pool.ReUse(emitters[i].transform.position, emitters[i].transform.rotation);//使用BubblePool的方法ReUse，把SetActive(false)的物件拿出來變true
            }
        }

        /// <summary>
        /// 多個發射口一起射，但是這個是用Instaniate，不是用物件池，可以自己丟東西進來 
        /// </summary>        
        public void MakeBubbleWithPools(GameObject prefab, GameObject[] emitters)
        {
            for (int i = 0; i < emitters.Length; i++)//多個發射口一齊發射
            {
                /*pool.ReUse(emitters[i].transform.position, emitters[i].transform.rotation);*///使用BubblePool的方法ReUse，把SetActive(false)的物件拿出來變true
                Instantiate(prefab, emitters[i].transform.position, emitters[i].transform.rotation);
            }
        }

        /// <summary>
        /// 發射大球的
        /// </summary>        
        public void MakeBigBallWithPools(GameObject[] emitters)
        {
            for (int i = 0; i < emitters.Length; i++)//多個發射口一齊發射
            {
                ballPool.ReUse(emitters[i].transform.position, emitters[i].transform.rotation);//使用BubblePool的方法ReUse，把SetActive(false)的物件拿出來變true
            }
        }

        /// <summary>
        /// 發射等速泡泡，做花樣用的
        /// </summary>       
        public void MakeBubbleWithSameSpeedPools(GameObject[] emitters)
        {
            for (int i = 0; i < emitters.Length; i++)//多個發射口一齊發射
            {
                sameSpeedPool.ReUse(emitters[i].transform.position, emitters[i].transform.rotation);//使用BubblePool的方法ReUse，把SetActive(false)的物件拿出來變true
            }
        }

        /// <summary>
        /// 只有一個樣式的大球製作方法，這個物件池只放一種大球
        /// </summary>
        public void MakeBigBallOnePatternWithPools(GameObject[] emitters)
        {
            for (int i = 0; i < emitters.Length; i++)//多個發射口一齊發射
            {
                bigBallOnePattern.ReUse(emitters[i].transform.position, emitters[i].transform.rotation);
            }
        }

        private void MakeInvisibleBubbleWithPools()
        {
            for (int i = 0; i < bubbleEmitter.Length; i++)//多個發射口一齊發射
            {
                laserColliderPool.ReUse(bubbleEmitter[i].transform.position, bubbleEmitter[i].transform.rotation);
            }
        }
        public void MakeInvisibleBubbleWithPools(GameObject[] emitters)
        {
            for (int i = 0; i < emitters.Length; i++)//多個發射口一齊發射
            {
                laserColliderPool.ReUse(emitters[i].transform.position, emitters[i].transform.rotation);
            }
        }
        public void MakeInvisibleBubbleWithPools(Transform[] emitters)
        {
            for (int i = 0; i < emitters.Length; i++)//多個發射口一齊發射
            {
                laserColliderPool.ReUse(emitters[i].transform.position, emitters[i].transform.rotation);
            }
        }
        #endregion


        //射出泡泡的各種方法
        public void BubbleShooting()
        {
            if (totalWaveShootingTimeCountDown >= 0)//如果每一波射出的總時間大於0
            {
                if (shootingTimeCountDown > 0)//但子彈射出倒數大於0的話
                {
                    shootingTimeCountDown -= Time.deltaTime;//子彈射出倒數會減少
                }
                else//子彈射出倒數小於0的話
                {
                    //在泡泡生成點生成一顆泡泡
                    MakeBubbleWithPools();
                    shootingTimeCountDown = resetShootingTime;//重新指派時間給子彈倒數
                }
                //回到上面那邊，整波射出時間>0的話，跑完上面那堆之後給他減少
                totalWaveShootingTimeCountDown -= Time.deltaTime;
            }
            else if (totalWaveShootingTimeCountDown < 0)//當整波的時間小於0
            {
                if (rechargeTimeCountDown > 0)//然後又，再充能時間的倒數大於0的時候
                {
                    rechargeTimeCountDown -= Time.deltaTime;//再充能時間倒數減少
                }
                else//再充能時間的倒數小於0的時候
                {
                    rechargeTimeCountDown = rechargeTime;//再充能時間倒數 重新指派一個時間給他
                    totalWaveShootingTimeCountDown = totalWaveShootingTime;//整波發射時間也給他重新指派
                }
            }
        }

        public void InvisibleBubbleShooting()
        {
            if (totalWaveShootingTimeCountDown >= 0)//如果每一波射出的總時間大於0
            {
                if (shootingTimeCountDown > 0)//但子彈射出倒數大於0的話
                {
                    shootingTimeCountDown -= Time.deltaTime;//子彈射出倒數會減少
                }
                else//子彈射出倒數小於0的話
                {
                    //在泡泡生成點生成一顆泡泡
                    MakeInvisibleBubbleWithPools();
                    shootingTimeCountDown = resetShootingTime;//重新指派時間給子彈倒數
                }
                //回到上面那邊，整波射出時間>0的話，跑完上面那堆之後給他減少
                totalWaveShootingTimeCountDown -= Time.deltaTime;
            }
            else if (totalWaveShootingTimeCountDown < 0)//當整波的時間小於0
            {
                if (rechargeTimeCountDown > 0)//然後又，再充能時間的倒數大於0的時候
                {
                    rechargeTimeCountDown -= Time.deltaTime;//再充能時間倒數減少
                }
                else//再充能時間的倒數小於0的時候
                {
                    rechargeTimeCountDown = rechargeTime;//再充能時間倒數 重新指派一個時間給他
                    totalWaveShootingTimeCountDown = totalWaveShootingTime;//整波發射時間也給他重新指派
                }
            }
        }

        //第一次攻擊時的延遲時間的Function
        public void FirstTimeShootCountDown()
        {
            if (firstShootingCountDown > 0)//如果第一次射出時間也大於0
            {
                firstShootingCountDown -= Time.deltaTime;//則第一次射出時間會減少
            }
            else//如果第一次射出時間小於0
            {
                return;
            }
        }

        #region //大魔王射泡泡的各種方法
        public void LastBossJumpShoot()
        {
            MakeBubbleWithPools(data.go_LBJumpShoot);
        }
        public void LastBossShootCircle()
        {
            MakeBubbleWithSameSpeedPools(data.go_LBShootCircle);
        }
        public void LastBossShootKome()
        {
            MakeBubbleWithPools(data.go_LBShootKome);
        }
        public void LastBossShootStar()
        {
            MakeBubbleWithPools(data.go_LBShootStar);
        }
        public void LastBossShootTriangle()
        {
            MakeBubbleWithPools(data.go_LBShootTriangle);
        }
        public void LastBossShootBigBall()
        {
            //MakeBubbleWithPools(bigBall, data.go_LBShootBigBall);
            //MakeBubbleWithPools(data.go_LBShootBigBall);
            MakeBigBallWithPools(data.go_LBShootBigBall);
        }
        public void LastBossShootBigBall2()
        {
            MakeBigBallWithPools(data.go_LBShootBigBall2);
        }
        public void LastBossShootAttack1()
        {
            MakeBubbleWithPools(data.go_LBShootAttack1);
        }
        public void LastBossJumpSameSpeed()
        {
            MakeBubbleWithSameSpeedPools(data.go_LBJumpShoot);
        }
        public void LastBossShootBigBigTernado()
        {
            MakeBigBallOnePatternWithPools(data.go_LBShootBigBallTernado);
        }
        #endregion


    }

}