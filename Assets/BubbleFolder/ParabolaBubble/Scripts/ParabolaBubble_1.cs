﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Bubble;

public class ParabolaBubble_1 : MonoBehaviour
{
    //位移
    /// <summary>
    /// 目標物
    /// </summary>
    public GameObject controller;
    /// <summary>
    /// 玩家位置，每次從物件池取出時要記憶一次
    /// </summary>
    public Vector3 v_controllerPos;
    /// <summary>
    /// 瞄準玩家的前方還是什麼的，的那個輔助向量
    /// </summary>
    public Vector3 v_targetPosToControllerPos;
    public float f_parabolaDeltatime;
    public Rigidbody rigidbody;
    /// <summary>
    /// X軸速度
    /// </summary>
    public float f_SpeedX = 5f;
    /// <summary>
    /// 到玩家的水平距離
    /// </summary>
    float f_distanceToPlayerX = 0;
    /// <summary>
    /// 到達時間
    /// </summary>
    public float f_arrivalTime = 0;
    /// <summary>
    /// 落下距離
    /// </summary>
    public float f_dropDownDistance = 0;
    /// <summary>
    /// 要噴多高的距離
    /// </summary>
    public Vector3 v_araisePosition = Vector3.zero;
    /// <summary>
    /// 斜邊的長度
    /// </summary>
    public float f_hypotenuseLength = 0;
    /// <summary>
    /// 角度
    /// </summary>
    public float f_angleToShoot = 0;
    /// <summary>
    /// 斜邊的速度計算
    /// </summary>
    public float f_hypotenuseSpeed = 0;
    /// <summary>
    /// 射出自己的爸爸
    /// </summary>
    public GameObject parabolaShooter;

    //命中特效物件池
    public BubbleHitEffectPool bubbleHitEffectPool;
    public BubbleHitGroundEffectPool bubbleHitGroundEffectPool;
    public ParabolaHitGroundEffectPool parabolaHitGroundEffectPool;
    public ParabolaHitPlayerEffectPool parabolaHitPlayerEffectPool;

    //回收物件池
    /// <summary>
    /// 回收物件池
    /// </summary>
    public ParabolaBubblePool parabolaBubblePool;

    //泡泡的生命
    /// <summary>
    /// 自己用的計時器，會記錄出生的瞬間的時刻
    /// </summary>
    public float _timer;
    /// <summary>
    /// 當出生的時刻加上這個時間之後，就代表泡泡要被回收了，所以這個是他的生命
    /// </summary>
    public float bubbleLife = 20.0f;

    // Start is called before the first frame update
    void Awake()
    {
        controller = GameObject.FindGameObjectWithTag("Controller");
        v_controllerPos = controller.transform.position;
        rigidbody = GetComponent<Rigidbody>();
        parabolaBubblePool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<ParabolaBubblePool>();
        bubbleHitEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<BubbleHitEffectPool>();
        bubbleHitGroundEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<BubbleHitGroundEffectPool>();
        parabolaHitGroundEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<ParabolaHitGroundEffectPool>();
        parabolaHitPlayerEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<ParabolaHitPlayerEffectPool>();

        //ColorChange();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        BubbleLifeReturnToPool();
        BubbleHitGround();
        //ParabolaMove();
    }

    public void OnEnable()
    {
        _timer = Time.time;
        //v_controllerPos = controller.transform.position;
        //v_targetPosToControllerPos = v_controllerPos + new Vector3(Random.Range(1.0f, 3.0f), 0.0f, Random.Range(0.0f, 2.0f));

        //transform.position = Vector3.zero;
        //transform.position = objectPool.transform.position;
        //transform.position = transform.position;
        //ParabolaMove();
        //rigidbody.AddForce(transform.forward * f_hypotenuseSpeed, ForceMode.Impulse);
    }

    #region BubbleColor
    Color[] _color;
    public void ColorChange()
    {
        _color = new Color[100];

        for (int i = 0; i < _color.Length; i++)
        {
            _color[i].r = Random.Range(0.0f, 0.3f);
            _color[i].g = Random.Range(0.0f, 0.5f);
            _color[i].b = Random.Range(0.0f, 1.0f);
            this.transform.GetComponent<MeshRenderer>().material.SetColor("_base_color", _color[i]);
        }
    }

    #endregion

    #region BubbleLife
    /// <summary>
    /// 時間到泡泡回收至物件池
    /// </summary>
    void BubbleLifeReturnToPool()//新版的回到物件池的泡泡
    {
        //下面這段if，應該只是防呆，有測試過如果刪掉還是一樣可以執行
        if (!gameObject.activeInHierarchy) { return; }
        if (Time.time > _timer + bubbleLife)//如果現在的遊戲時間大於 物件被Active時的時間+泡泡生命時，把泡泡回收
        {
            //transform.position = Vector3.zero;
            //parabolaBubblePool.Recovery(this.gameObject);
            Destroy(gameObject, 0.2f);
        }
    }

    public void BubbleHitGround()
    {
        if (this.transform.position.y <= controller.transform.position.y + 0.1f && this.transform.position.y >= controller.transform.position.y - 0.1f)
        {
            //transform.position = Vector3.zero;
            //bubbleHitGroundEffectPool.ReUse(this.gameObject.transform.position, Quaternion.Euler(0.0f, 0.0f, 0.0f));
            //parabolaBubblePool.Recovery(this.gameObject);
            parabolaHitGroundEffectPool.ReUse(transform.position + new Vector3(0.0f, 0.1f, 0.0f), Quaternion.identity);
            Destroy(gameObject, 0.5f);
        }
    }
    #endregion

    #region BubbleMove
    public void ParabolaMove()
    {
        //計算到玩家的平面距離
        f_distanceToPlayerX = Vector3.Distance(new Vector3( v_controllerPos.x, 0 ,v_controllerPos.z), new Vector3(transform.position.x, 0, transform.position.z));
        Debug.Log("到玩家距離：" + f_distanceToPlayerX);
        //到達時間的計算 = 距離除以速度
        f_arrivalTime = f_distanceToPlayerX / f_SpeedX;
        //落下距離 用Abs求絕對值
        f_dropDownDistance = Mathf.Abs( 0.5f * Physics.gravity.y * f_arrivalTime * f_arrivalTime);
        Debug.Log("落下距離：" + f_dropDownDistance);
        //計算要噴多高的向量，到達點的上方
        v_araisePosition = v_controllerPos + Vector3.up * f_dropDownDistance;
        //計算斜邊長度
        f_hypotenuseLength = Vector3.Distance(transform.position, v_araisePosition);
        Debug.Log("斜邊長度：" + f_hypotenuseLength);
        //角度計算 (斜邊的平方+底邊的平方-垂直邊的平方)除以(2*斜邊*底邊)
        f_angleToShoot = Mathf.Acos((Mathf.Pow(f_hypotenuseLength, 2) + Mathf.Pow(f_distanceToPlayerX, 2) - Mathf.Pow(f_dropDownDistance, 2))
            / (2 * f_hypotenuseLength * f_distanceToPlayerX));
        //斜邊的速度計算 = 斜邊長度除以到達時間
        f_hypotenuseSpeed = f_hypotenuseLength / f_arrivalTime;
        Debug.Log("斜邊速度：" + f_hypotenuseSpeed);

        //rigidbody.AddForce(transform.forward * f_hypotenuseSpeed, ForceMode.Impulse);
    }

    public void ParabolaMove2()
    {

    }
    #endregion

    #region Bubble Attack

    public bool bubbleHitPlayer = false;

    public void OnTriggerEnter(Collider other)//這邊寫泡泡撞到的東西會怎樣
    {//這邊只寫，泡泡撞到泡泡不會毀滅，泡泡不會互相影響
     //print(other.gameObject.name);

        if (other.gameObject.layer == LayerMask.NameToLayer("Bubble"))
        {
            //不做任何事
        }
        if (other.gameObject.layer == LayerMask.NameToLayer("Default"))
        {
            //transform.position = Vector3.zero;
            //parabolaBubblePool.Recovery(this.gameObject);
            parabolaHitGroundEffectPool.ReUse(transform.position + new Vector3(0.0f, 0.1f, 0.0f), Quaternion.identity);
            Destroy(gameObject, 0.3f);
        }
        else if (other.gameObject.layer == LayerMask.NameToLayer("Bullet"))//打到子彈圖層，應該還要產生動畫
        {
            //transform.position = Vector3.zero;
            //parabolaBubblePool.Recovery(this.gameObject);//回收泡泡，this=泡泡
            //Destroy(gameObject, 0.2f);
        }
        else if (other.gameObject.layer == LayerMask.NameToLayer("Player"))//打到玩家圖層
        {
            //transform.position = Vector3.zero;
            //parabolaBubblePool.Recovery(this.gameObject);//回收泡泡，this=泡泡
            Destroy(gameObject, 0.2f);
        }
        else if (other.gameObject.layer == LayerMask.NameToLayer("Controller"))//打到玩家圖層
        {
            //transform.position = Vector3.zero;
            //bubbleHitEffectPool.ReUse(this.transform.position, Quaternion.identity);
            //parabolaBubblePool.Recovery(this.gameObject);//回收泡泡，this=泡泡
            parabolaHitPlayerEffectPool.ReUse(transform.position+new Vector3(0.0f, 0.1f, 0.0f), Quaternion.identity);
            Destroy(gameObject, 0.5f);
        }
        else if (other.gameObject.tag == ("Weapon"))//打到武器圖層
        {
            //transform.position = Vector3.zero;
            //parabolaBubblePool.Recovery(this.gameObject);//回收泡泡，this=泡泡
            Destroy(gameObject, 0.2f);
        }
    }
    #endregion
}
