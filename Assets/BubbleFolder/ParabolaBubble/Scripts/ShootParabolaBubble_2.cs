﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootParabolaBubble_2 : MonoBehaviour
{
    //　弾の到達点
    [SerializeField]
    private Transform arrivalPoint;
    //　弾の横軸の速さ
    [SerializeField]
    private float speed = 10f;
    //　弾を消すまでの時間
    [SerializeField]
    private float deleteTime = 10f;
    //　弾が移動している時間
    private float totalTime;
    //　弾のプレハブ
    [SerializeField]
    private GameObject bullet;
    //　弾のインスタンス
    private GameObject bulletIns;
    //　弾を飛ばす場所
    [SerializeField]
    private Transform bulletHole;
    //　弾の到達時間
    private float arrivalTime;
    //　弾の角度
    private float theta;
    //　弾の斜辺の長さ
    private float hypotenuse;
    //　斜辺の速さ
    private float hypotenuseSpeed;
    //　重力で移動する距離
    private float gravityY;

    private void Start()
    {
        //bulletHole = transform.Find("BulletHole");
        Shot();
    }

    private void Update()
    {
        if (bulletIns != null)
        {
            //　経過時間計測
            totalTime += Time.deltaTime;
            //　重力で移動する距離を計算
            gravityY = 0.5f * Physics.gravity.y * totalTime * totalTime;
            //　弾の位置を斜線方向の距離＋重力で移動する距離を足して求める
            bulletIns.transform.position = bulletIns.transform.forward * hypotenuseSpeed * totalTime + new Vector3(0f, gravityY, 0f);
        }
    }

    private void Shot()
    {
        //　底辺の長さを計算
        var adjacent = Vector3.Distance(transform.position, arrivalPoint.position);
        Debug.Log(adjacent);
        //　到着時間の計算
        arrivalTime = adjacent / speed;
        //　落下距離を計算
        var opposite = Mathf.Abs(0.5f * Physics.gravity.y * arrivalTime * arrivalTime);
        Debug.Log(opposite);
        //　到達点＋上向きに落下距離
        var upPos = arrivalPoint.position + Vector3.up * opposite;
        //　斜辺の長さを計算
        var hypotenuse = Vector3.Distance(transform.position, upPos);
        Debug.Log(hypotenuse);
        //　角度を計算
        theta = -Mathf.Acos((Mathf.Pow(hypotenuse, 2) + Mathf.Pow(adjacent, 2) - Mathf.Pow(opposite, 2)) / (2 * hypotenuse * adjacent));
        //　一旦攻撃対象の方を見る
        transform.LookAt(arrivalPoint.position);
        //　砲台の向きを変える
        transform.Rotate(Vector3.right, theta * Mathf.Rad2Deg, Space.Self);
        //　弾のインスタンス化
        bulletIns = Instantiate(bullet, bulletHole.position, bulletHole.rotation);
        //　時間が経ったら削除する
        Destroy(bulletIns, deleteTime);
        //　横軸のspeedから斜め方向の速さを計算
        hypotenuseSpeed = hypotenuse / arrivalTime;
        Debug.Log(hypotenuseSpeed);

        //　砲台から到達点に線を引く
        Debug.DrawLine(transform.position, arrivalPoint.position, Color.red, deleteTime);
        //　砲台から到達点の上（重力が働いていない時の到達場所）に線を引く
        Debug.DrawLine(transform.position, arrivalPoint.position + new Vector3(0f, opposite, 0f), Color.yellow, deleteTime);
        //　砲台と到達点の上の半分の位置から到達点に線を引く
        Debug.DrawLine((arrivalPoint.position + new Vector3(0f, opposite, 0f) - transform.position) / 2f, arrivalPoint.position, Color.gray, deleteTime);
    }
}
