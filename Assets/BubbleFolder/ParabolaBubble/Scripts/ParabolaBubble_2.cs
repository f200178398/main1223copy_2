﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Bubble;

public class ParabolaBubble_2 : MonoBehaviour
{
    //位移
    /// <summary>
    /// 目標物
    /// </summary>
    public GameObject controller;
    /// <summary>
    /// 玩家位置，每次從物件池取出時要記憶一次
    /// </summary>
    public Vector3 v_controllerPos;
    /// <summary>
    /// 瞄準玩家的前方還是什麼的，的那個輔助向量
    /// </summary>
    public Vector3 v_targetPosToControllerPos;
    public float f_parabolaDeltatime;
    public Rigidbody rigidbody;
    /// <summary>
    /// X軸速度
    /// </summary>
    public float f_SpeedX = 5f;
    /// <summary>
    /// 到玩家的水平距離
    /// </summary>
    float f_distanceToPlayerX = 0;
    /// <summary>
    /// 到達時間
    /// </summary>
    public float f_arrivalTime = 0;
    /// <summary>
    /// 落下距離
    /// </summary>
    public float f_dropDownDistance = 0;
    /// <summary>
    /// 要噴多高的距離
    /// </summary>
    public Vector3 v_araisePosition = Vector3.zero;
    /// <summary>
    /// 斜邊的長度
    /// </summary>
    public float f_hypotenuseLength = 0;
    /// <summary>
    /// 角度
    /// </summary>
    public float f_angleToShoot = 0;
    /// <summary>
    /// 斜邊的速度計算
    /// </summary>
    public float f_hypotenuseSpeed = 0;
    /// <summary>
    /// 射出自己的爸爸
    /// </summary>
    public GameObject parabolaShooter;

    //命中特效物件池
    public BubbleHitEffectPool bubbleHitEffectPool;
    public BubbleHitGroundEffectPool bubbleHitGroundEffectPool;

    //回收物件池
    /// <summary>
    /// 回收物件池
    /// </summary>
    public ParabolaBubblePool parabolaBubblePool;

    //泡泡的生命
    /// <summary>
    /// 自己用的計時器，會記錄出生的瞬間的時刻
    /// </summary>
    public float _timer;
    /// <summary>
    /// 當出生的時刻加上這個時間之後，就代表泡泡要被回收了，所以這個是他的生命
    /// </summary>
    public float bubbleLife = 20.0f;

    // Start is called before the first frame update
    void Awake()
    {
        controller = GameObject.FindGameObjectWithTag("Controller");
        v_controllerPos = controller.transform.position;
        rigidbody = GetComponent<Rigidbody>();
        parabolaBubblePool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<ParabolaBubblePool>();
        bubbleHitEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<BubbleHitEffectPool>();
        bubbleHitGroundEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<BubbleHitGroundEffectPool>();

        ColorChange();

        
    }

    private void Start()
    {
        v_startSpeedVector = Quaternion.Euler(new Vector3(0, 0, angle)) * Vector3.right * power;
        v_currentAngle = Vector3.zero;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        BubbleLifeReturnToPool();
        BubbleHitGround();
        //ParabolaMove();
        //ParabolaMove_Test();
        ParabolaMove();
    }

    public void OnEnable()
    {
        _timer = Time.time;
        //v_controllerPos = controller.transform.position;
        //v_targetPosToControllerPos = v_controllerPos + new Vector3(Random.Range(1.0f, 3.0f), 0.0f, Random.Range(0.0f, 2.0f));

        //transform.position = Vector3.zero;
        //transform.position = objectPool.transform.position;
        //transform.position = transform.position;
        v_startSpeedVector = Quaternion.Euler(new Vector3(0, 0, angle)) * Vector3.right * power;
        v_currentAngle = Vector3.zero;

    }

    #region BubbleColor
    Color[] _color;
    public void ColorChange()
    {
        _color = new Color[100];

        for (int i = 0; i < _color.Length; i++)
        {
            _color[i].r = Random.Range(0.0f, 0.3f);
            _color[i].g = Random.Range(0.0f, 0.5f);
            _color[i].b = Random.Range(0.0f, 1.0f);
            this.transform.GetComponent<MeshRenderer>().material.SetColor("_base_color", _color[i]);
        }
    }

    #endregion

    #region BubbleLife
    /// <summary>
    /// 時間到泡泡回收至物件池
    /// </summary>
    void BubbleLifeReturnToPool()//新版的回到物件池的泡泡
    {
        //下面這段if，應該只是防呆，有測試過如果刪掉還是一樣可以執行
        if (!gameObject.activeInHierarchy) { return; }
        if (Time.time > _timer + bubbleLife)//如果現在的遊戲時間大於 物件被Active時的時間+泡泡生命時，把泡泡回收
        {
            parabolaBubblePool.Recovery(this.gameObject);
        }
    }

    public void BubbleHitGround()
    {
        if (this.transform.position.y <= controller.transform.position.y + 0.2f && this.transform.position.y >= controller.transform.position.y - 0.2f)
        {
            bubbleHitGroundEffectPool.ReUse(this.gameObject.transform.position, Quaternion.Euler(0.0f, 0.0f, 0.0f));
            parabolaBubblePool.Recovery(this.gameObject);
        }
    }
    #endregion

    #region BubbleMove
    //Vector3 pos;
    //int a=1;

    //public void ParabolaMove_Test()
    //{
    //    pos.x += 0.03f * a;
    //    if (pos.x > 5)
    //    {
    //        a = -1;
    //    }
    //    else if(pos.x< -5)
    //    {
    //        a = 1;
    //    }
    //    pos.y = 0.5f * Mathf.Pow(pos.x, 2);
    //    transform.position = pos;
    //}
    public float power = 20f;
    public float angle = 60f;
    public float gravatity = Physics.gravity.y;

    private Vector3 v_startSpeedVector;
    private Vector3 v_gravitySpeed = Vector3.zero;
    private float dTime;
    private Vector3 v_currentAngle;

    public void ParabolaMove()
    {
        v_gravitySpeed.y = gravatity * (dTime += Time.fixedDeltaTime);
        transform.position += (v_startSpeedVector + v_gravitySpeed) * Time.fixedDeltaTime;
        v_currentAngle.z = Mathf.Atan((v_startSpeedVector.y+ v_gravitySpeed.y) / v_startSpeedVector.x) * Mathf.Rad2Deg;
        transform.eulerAngles = v_currentAngle;
    }


    #endregion

    #region Bubble Attack

    public bool bubbleHitPlayer = false;

    public void OnTriggerEnter(Collider other)//這邊寫泡泡撞到的東西會怎樣
    {//這邊只寫，泡泡撞到泡泡不會毀滅，泡泡不會互相影響
     //print(other.gameObject.name);

        if (other.gameObject.layer == LayerMask.NameToLayer("Bubble"))
        {
            //不做任何事
        }
        if (other.gameObject.layer == LayerMask.NameToLayer("Default"))
        {
            parabolaBubblePool.Recovery(this.gameObject);
        }
        else if (other.gameObject.layer == LayerMask.NameToLayer("Bullet"))//打到子彈圖層，應該還要產生動畫
        {
            parabolaBubblePool.Recovery(this.gameObject);//回收泡泡，this=泡泡
        }
        else if (other.gameObject.layer == LayerMask.NameToLayer("Player"))//打到玩家圖層
        {
            parabolaBubblePool.Recovery(this.gameObject);//回收泡泡，this=泡泡
        }
        else if (other.gameObject.layer == LayerMask.NameToLayer("Controller"))//打到玩家圖層
        {
            bubbleHitEffectPool.ReUse(this.transform.position, Quaternion.identity);
            parabolaBubblePool.Recovery(this.gameObject);//回收泡泡，this=泡泡
        }
        else if (other.gameObject.tag == ("Weapon"))//打到武器圖層
        {

            parabolaBubblePool.Recovery(this.gameObject);//回收泡泡，this=泡泡
        }
    }
    #endregion


    public void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(transform.position, transform.position + transform.forward);
        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position, transform.position + transform.right);
    }
}
