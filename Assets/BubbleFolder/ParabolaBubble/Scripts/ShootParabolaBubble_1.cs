﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SA;

public class ShootParabolaBubble_1 : MonoBehaviour
{
    /*
     * 重大BUG，子彈回收至物件池之後再拿出來，因為回收前的力量仍殘留，所以第二次會沒有辦法射出拋物線
      */


    //　弾の到達点
    //追蹤玩家的點
    [SerializeField]
    private Transform arrivalPoint;
    public GameObject controller;
    public Vector3 aimmingPoint = new Vector3(2.0f, 0f, 2.0f);
    //瞄準的點的範圍
    public float aimingPointRange=0f;
    //　弾の横軸の速さ
    //這個就是控制速度的最重要的速度
    //一定要10以上
    [SerializeField]
    private float speed = 10f;
    //　弾を消すまでの時間
    //子彈的生命，其實可以不用設定，因為泡泡身上自有腳本回收
    //[SerializeField]
    private float deleteTime = 10f;
    //　弾が移動している時間
    private float totalTime;
    //　弾のプレハブ
    [SerializeField]
    //private GameObject bullet;
    //　弾のインスタンス
    //private GameObject bulletIns;
    public GameObject go_Parabolabubble;
    //　弾を飛ばす場所
    //Emitter
    [SerializeField]
    private Transform bulletHole;
    //　弾の到達時間
    private float arrivalTime;
    //　弾の角度
    private float theta;
    //　弾の斜辺の長さ
    private float hypotenuse;
    //　斜辺の速さ
    private float hypotenuseSpeed;
    //　重力で移動する距離
    private float gravityY;
    //　経過時間
    private float elapsedTime = 0f;
    //　弾を発射するまでの時間間隔
    //一直發射的速度
    [SerializeField]
    private float durationTime = 0.5f;
    public ParabolaBubblePool parabolaBubblePool;

    //開關
    public AI_data data;
    public bool b_startShoot;
    public EnemyTarget enemyTarget;
    public FightSystem fightSystem;

    private void Start()
    {
        //bulletHole = transform.Find("BulletHole");
        controller = GameObject.FindGameObjectWithTag("Controller");
        arrivalPoint = controller.transform;
        parabolaBubblePool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<ParabolaBubblePool>();
        data = GameObject.Find("Boss").GetComponent<AI_data>();
        enemyTarget = GameObject.Find("Boss").GetComponent<EnemyTarget>();
        fightSystem = GameObject.FindGameObjectWithTag("FightSystem").GetComponent<FightSystem>();
    }

    private void FixedUpdate()
    {
        ShootSwitch();
        StartShootAndShootSpeed();
    }

    private void Shot()
    {
        //給一個隨機的要瞄準離主角多少距離的點
        aimmingPoint = new Vector3(Random.Range(-aimingPointRange, aimingPointRange), 0f, Random.Range(-aimingPointRange, aimingPointRange));
        //　底辺の長さを計算
        var adjacent = Vector3.Distance(transform.position, arrivalPoint.position + aimmingPoint);
        //Debug.Log(adjacent);
        //　到着時間の計算
        arrivalTime = adjacent / speed;
        //　落下距離を計算
        var opposite = Mathf.Abs(0.5f * Physics.gravity.y * arrivalTime * arrivalTime);
        //Debug.Log(opposite);
        //　到達点＋上向きに落下距離
        var upPos = (arrivalPoint.position + aimmingPoint) + Vector3.up * opposite;
        //　斜辺の長さを計算
        var hypotenuse = Vector3.Distance(transform.position, upPos);
        //Debug.Log(hypotenuse);
        //　角度を計算
        theta = -Mathf.Acos((Mathf.Pow(hypotenuse, 2) + Mathf.Pow(adjacent, 2) - Mathf.Pow(opposite, 2)) / (2 * hypotenuse * adjacent));
        //　一旦攻撃対象の方を見る
        transform.LookAt(arrivalPoint.position + aimmingPoint);
        //　砲台の向きを変える
        transform.Rotate(Vector3.right, theta * Mathf.Rad2Deg, Space.Self);
        //　弾のインスタンス化
        //bulletIns = Instantiate(bullet, bulletHole.position, bulletHole.rotation);
        go_Parabolabubble = parabolaBubblePool.ReUseObject(bulletHole.position, bulletHole.rotation);

        //　時間が経ったら削除する
        //Destroy(bulletIns, deleteTime);
        //　横軸のspeedから斜め方向の速さを計算
        hypotenuseSpeed = hypotenuse / arrivalTime;

        go_Parabolabubble.GetComponent<Rigidbody>().AddForce(go_Parabolabubble.transform.forward * hypotenuseSpeed, ForceMode.Impulse);
    }

    public void ShootSwitch()
    {
        if(enemyTarget.currentState == SA.StateType.LastBossDeath || data.GetHealthRate() <= 0.0f)
        {
            b_startShoot = false;
        }
        else if (fightSystem.currentHp<=0.0f)
        {
            b_startShoot = false;
        }
        else if (enemyTarget.currentState == SA.StateType.LastBossFirstShoot)
        {
            b_startShoot = true;
        }
    }

    public void StartShootAndShootSpeed()
    {
        elapsedTime += Time.deltaTime;
        if ( !b_startShoot) { return; }



        if (elapsedTime >= durationTime)
        {
            elapsedTime = 0f;
            Shot();
            //parabolaBubblePool.ReUse(transform.position, transform.rotation);
        }
    }
}
