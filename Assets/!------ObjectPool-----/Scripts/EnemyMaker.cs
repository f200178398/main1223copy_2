﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Bubble;
using SA;
public class EnemyMaker : MonoBehaviour
{
    [Header("確認有沒有Get到怪獸池")]
    public KylePool kylePool;
    public LongLegPool longLegPool;
    public SpiderPool spiderPool;
    public MixedEnemyPool mixedEnemyPool;
    public GameObject controller;
    public AI_data data;
    public SummonEffectPool summonEffectPool;
    public AI_data boss_data;
    public FightSystem fightSystem;

    [Header("Enemy Maker Active Settings")]
    public float f_enemyMakerActiveRange;
    public bool b_controllerInRange;
    public bool EnemyMakerActive()
    {        
        float distance = Vector3.Distance(controller.transform.position, this.transform.position);
        if (f_enemyMakerActiveRange == 0) { Debug.LogError("沒有設定啟動敵人製造機的距離"); }
        if (distance < f_enemyMakerActiveRange)
        {
            b_controllerInRange = true;
            return true;

        }
        else if (data.currentHp <= 0)
        {
            b_controllerInRange = false;
            return false;
        }
        return false;
    }

    // Start is called before the first frame update
    void Awake()
    {
        kylePool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<KylePool>();
        longLegPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<LongLegPool>();
        spiderPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<SpiderPool>();
        mixedEnemyPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<MixedEnemyPool>();
        data = GetComponentInParent<AI_data>();
        summonEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<SummonEffectPool>();
        boss_data = GameObject.Find("Boss").GetComponent<AI_data>();
        fightSystem = GameObject.FindGameObjectWithTag("FightSystem").GetComponent<FightSystem>();

        controller = GameObject.FindGameObjectWithTag("Controller");
    }


    public void Start()
    {
        f_kyleSpawnTime = Random.Range(15f, 30f);
        f_enemyMakerActiveRange = Random.Range(25f, 40f);
    }
    // Update is called once per frame
    void FixedUpdate()
    {

        EnemyMakerActive();
        if (EnemyMakerActive() || b_controllerInRange)
        {
            if (boss_data.GetHealthRate() <= 0.1f || fightSystem.currentHp <=0) { return; }
            StartMakeEnemies();
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            MakeKyle();
            MakeLongLeg();
            MakeSpider();
            MakeMixedEnemy();
        }
        ShowError();
    }
    [Header("召喚陣的特效相關")]
    [Tooltip("真正召喚怪獸的時間= f_kyleSpawnTime + f_makeSummonTime")]
    public float f_summonTimer;
    public float f_makeSummonTime = 0.5f;


    [Header("生產哪些怪物")]
    public bool b_makeKyle;
    public bool b_makeLongLeg;
    public bool b_makeSpider;
    public bool b_makeMixedEnemyButNotSoMany;

    [Header("Qauntity Settings")]
    public bool b_enemyHasLimitedQauntity;
    public int i_howManyEnemies;
    public int i_howManyKyles;
    public int i_howManyLongLegs;
    public int i_howManySpiders;
    [SerializeField] private int i_nowEnemies;
    [SerializeField] private int i_nowKyles;
    [SerializeField] private int i_nowLongLegs;
    [SerializeField] private int i_nowSpiders;

    [Header("Spawn Times Settings")]
    public bool b_togetherSpawn;
    private float f_timer;
    public float f_togetherSpawnTimes;
    private float f_kyleSpawnTimer;
    public float f_kyleSpawnTime;
    private float f_LongLegSpawnTimer;
    public float f_LongLegSpawnTime;
    private float f_SpiderSpawnTimer;
    public float f_SpiderSpawnTime;
    private float f_MixedEnemySpawnTimer;
    public float f_MixedEnemySpawnTime;

    [Header("Spawn Position Settings")]
    public GameObject[] goArray_spawnPosition;
    [HideInInspector] public bool b_randomPositionBaseGOPosition;
    [HideInInspector] public float f_randomPositionRangeX;
    [HideInInspector] public float f_randomPositionRangeZ;
    public bool b_everyPositionSameTimeSpawn;
    public int i_positionIndex;
    #region 產生怪物的方法
    /// <summary>
    /// 隨機產生地點，回傳的是本地位置+一個向量
    /// </summary>
    public Vector3 RandomPostionMaker()
    {
        if (b_randomPositionBaseGOPosition == false) { return Vector3.zero; }

        float x, y, z;
        x = Random.Range(0.0f, f_randomPositionRangeX);
        y = 0;
        z = Random.Range(0.0f, f_randomPositionRangeZ);

        Vector3 newPosition = new Vector3(x, y, z);
        this.transform.position = this.transform.position + newPosition;

        return this.transform.position;
    }


    /// <summary>
    /// 製造凱爾
    /// </summary>
    public void MakeKyle()
    {
        if (b_makeKyle == false) { return; }

        //kylePool.ReUse(this.transform.position + RandomPostionMaker(), this.transform.rotation);
        if (b_everyPositionSameTimeSpawn)
        {
            for (int i = 0; i < goArray_spawnPosition.Length; i++)
            {
                kylePool.ReUse(goArray_spawnPosition[i].transform.position, goArray_spawnPosition[i].transform.rotation);
            }
        }
        else if (b_everyPositionSameTimeSpawn == false)
        {

            kylePool.ReUse(goArray_spawnPosition[i_positionIndex].transform.position, goArray_spawnPosition[i_positionIndex].transform.rotation);
            i_positionIndex++;
            if (i_positionIndex >= goArray_spawnPosition.Length)
            {
                i_positionIndex = 0;
            }
        }


    }

    public IEnumerator MakeKyle(float delayTime)
    {
        yield return new WaitForSeconds(delayTime);



        //kylePool.ReUse(this.transform.position + RandomPostionMaker(), this.transform.rotation);
        if (b_everyPositionSameTimeSpawn)
        {
            for (int i = 0; i < goArray_spawnPosition.Length; i++)
            {
                kylePool.ReUse(goArray_spawnPosition[i].transform.position, goArray_spawnPosition[i].transform.rotation);
            }
        }
        else if (b_everyPositionSameTimeSpawn == false)
        {

            kylePool.ReUse(goArray_spawnPosition[i_positionIndex].transform.position, goArray_spawnPosition[i_positionIndex].transform.rotation);
            i_positionIndex++;
            if (i_positionIndex >= goArray_spawnPosition.Length)
            {
                i_positionIndex = 0;
            }
        }
    }

    /// <summary>
    /// 製造長腳機器人
    /// </summary>
    public void MakeLongLeg()
    {
        if (b_makeLongLeg == false) { return; }

        if (b_everyPositionSameTimeSpawn)
        {
            for (int i = 0; i < goArray_spawnPosition.Length; i++)
            {
                longLegPool.ReUse(goArray_spawnPosition[i].transform.position, goArray_spawnPosition[i].transform.rotation);
            }
        }
        else if (b_everyPositionSameTimeSpawn == false)
        {

            longLegPool.ReUse(goArray_spawnPosition[i_positionIndex].transform.position, goArray_spawnPosition[i_positionIndex].transform.rotation);
            i_positionIndex++;
            if (i_positionIndex >= goArray_spawnPosition.Length)
            {
                i_positionIndex = 0;
            }
        }

    }
    /// <summary>
    /// 製造四腳機器人
    /// </summary>
    public void MakeSpider()
    {
        if (b_makeSpider == false) { return; }

        if (b_everyPositionSameTimeSpawn)
        {
            for (int i = 0; i < goArray_spawnPosition.Length; i++)
            {
                spiderPool.ReUse(goArray_spawnPosition[i].transform.position, goArray_spawnPosition[i].transform.rotation);
            }
        }
        else if (b_everyPositionSameTimeSpawn == false)
        {

            spiderPool.ReUse(goArray_spawnPosition[i_positionIndex].transform.position, goArray_spawnPosition[i_positionIndex].transform.rotation);
            i_positionIndex++;
            if (i_positionIndex >= goArray_spawnPosition.Length)
            {
                i_positionIndex = 0;
            }
        }

    }

    /// <summary>
    /// 隨機製造，速度不會太快
    /// </summary>
    public void MakeMixedEnemy()
    {
        if (b_makeMixedEnemyButNotSoMany == false) { return; }

        if (b_everyPositionSameTimeSpawn)
        {
            for (int i = 0; i < goArray_spawnPosition.Length; i++)
            {
                mixedEnemyPool.ReUse(goArray_spawnPosition[i].transform.position, goArray_spawnPosition[i].transform.rotation);
            }
        }
        else if (b_everyPositionSameTimeSpawn == false)
        {

            mixedEnemyPool.ReUse(goArray_spawnPosition[i_positionIndex].transform.position, goArray_spawnPosition[i_positionIndex].transform.rotation);
            i_positionIndex++;
            if (i_positionIndex >= goArray_spawnPosition.Length)
            {
                i_positionIndex = 0;
            }
        }
    }
    #endregion

    #region 生產怪物，放到update裡面就會一直生產
    public void StartMakeEnemies()
    {
        if (b_togetherSpawn)
        {
            //數量大於定數後跳出
            if (b_enemyHasLimitedQauntity)
            {
                if (i_nowEnemies >= i_howManyEnemies)
                {
                    return;
                }
            }


            //計時到之後生出
            f_timer += Time.deltaTime;


            if (f_timer > f_togetherSpawnTimes)
            {

                summonEffectPool.ReUse(transform.position, transform.rotation * Quaternion.Euler(+90.0f, 0f, 0f));


                //MakeKyle();
                StartCoroutine(MakeKyle(1.0f));
                MakeLongLeg();
                MakeSpider();
                MakeMixedEnemy();

                i_nowEnemies++;


                f_timer = 0;//計時器歸0

            }
        }
        else
        {
            if (b_makeKyle)
            {
                f_kyleSpawnTimer += Time.deltaTime;
                if (b_enemyHasLimitedQauntity)
                {
                    if (i_nowKyles >= i_howManyKyles)
                    {
                        return;
                    }
                }
                if (f_kyleSpawnTimer > f_kyleSpawnTime)
                {

                    summonEffectPool.ReUse(transform.position, transform.rotation * Quaternion.Euler(+90.0f, 0f, 0f));

                    //MakeKyle();
                    StartCoroutine(MakeKyle(1.0f));
                    i_nowKyles++;






                    f_kyleSpawnTimer = 0;
                }
            }
            else if (b_makeLongLeg)
            {
                f_LongLegSpawnTimer += Time.deltaTime;
                if (b_enemyHasLimitedQauntity)
                {
                    if (i_nowLongLegs >= i_howManyLongLegs)
                    {
                        return;
                    }
                }
                if (f_LongLegSpawnTimer > f_LongLegSpawnTime)
                {
                    MakeLongLeg();

                    i_nowLongLegs++;

                    f_LongLegSpawnTimer = 0;
                }
            }
            else if (b_makeSpider)
            {
                f_SpiderSpawnTimer += Time.deltaTime;
                if (b_enemyHasLimitedQauntity)
                {
                    if (i_nowSpiders >= i_howManySpiders)
                    {
                        return;
                    }
                }
                if (f_SpiderSpawnTimer > f_SpiderSpawnTime)
                {
                    MakeSpider();

                    i_nowSpiders++;

                    f_SpiderSpawnTimer = 0;
                }
            }
            else if (b_makeMixedEnemyButNotSoMany)
            {
                f_MixedEnemySpawnTimer += Time.deltaTime;
                if (b_enemyHasLimitedQauntity)
                {
                    if (i_nowEnemies >= i_howManyEnemies)
                    {
                        return;
                    }
                }
                if (f_MixedEnemySpawnTimer > f_MixedEnemySpawnTime)
                {
                    MakeMixedEnemy();

                    i_howManyEnemies++;

                    f_MixedEnemySpawnTimer = 0;
                }
            }
        }

    }
    #endregion

    #region 防呆
    private void ShowError()
    {
        if (!b_makeKyle && !b_makeLongLeg && !b_makeSpider)
        {
            Debug.LogError("要生產什麼怪獸沒打勾");
        }

        if (goArray_spawnPosition.Length == 0)
        {
            Debug.LogError("沒有給出生點");
        }



        if (b_togetherSpawn && f_togetherSpawnTimes == 0)
        {
            Debug.LogError("生成時間0秒一秒鐘生成60隻");
            if (i_howManyEnemies == 0)
            {
                Debug.LogError("生成數量0隻");
            }
        }
        else if (!b_togetherSpawn && b_makeKyle && f_kyleSpawnTime == 0)
        {
            Debug.LogError("凱爾 的生成時間0秒一秒鐘生成60隻");
            if (i_howManyKyles == 0)
            {
                Debug.LogError("凱爾的生成數量0隻");
            }
        }
        else if (!b_togetherSpawn && b_makeLongLeg && f_LongLegSpawnTime == 0)
        {
            Debug.LogError("長腳 的生成時間0秒一秒鐘生成60隻");
            if (i_howManyLongLegs == 0)
            {
                Debug.LogError("長腳的 生成數量0隻");
            }
        }
        else if (!b_togetherSpawn && b_makeSpider && f_SpiderSpawnTime == 0)
        {
            Debug.LogError("四腳 的生成時間0秒一秒鐘生成60隻");
            if (i_howManySpiders == 0)
            {
                Debug.LogError("四腳的 生成數量0隻");
            }
        }
        else if (!b_togetherSpawn && b_makeMixedEnemyButNotSoMany && f_MixedEnemySpawnTime == 0)
        {
            Debug.LogError("混合 的生成時間0秒一秒鐘生成60隻");
            if (i_howManyEnemies == 0)
            {
                Debug.LogError("生成數量0隻");
            }
        }
    }
    #endregion

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(transform.position, transform.position + transform.forward);
        if (!b_controllerInRange || !EnemyMakerActive())
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, f_enemyMakerActiveRange);
        }
        else
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, f_enemyMakerActiveRange);
        }
    }

}
