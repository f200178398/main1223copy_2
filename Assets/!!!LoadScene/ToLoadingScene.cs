﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ToLoadingScene : MonoBehaviour
{
    public string nextSceneName;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnPointerClick()
    {
        SceneManager.LoadSceneAsync(nextSceneName);

    }
}
