﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class LoadAsyncScene : MonoBehaviour
{
    private float progressValue;

    private Text progress;

    private Slider slider;

    public string nextSceneName;

    private AsyncOperation async = null;

    void Start()
    {
        progress = GetComponent<Text>();
        slider = FindObjectOfType<Slider>();
        StartCoroutine("LoadScene");

    }

    // Update is called once per frame
    void Update()
    {
       
    }

    IEnumerator LoadScene()
    {
        async = SceneManager.LoadSceneAsync(nextSceneName);
        async.allowSceneActivation = false;

        while (!async.isDone)
        {
            if(async.progress < 0.9f)
            {
                progressValue = async.progress;
            }
            else
            {
                progressValue = 1f;
            }

            slider.value = progressValue;
            progress.text = (int)(slider.value * 100) + "%";

            if (progressValue >= 0.9)
            {
                progress.text = "按任意键继续";
                if (Input.anyKeyDown)
                {
                    async.allowSceneActivation = true;
                }
            }

        }

         yield return null;
    }
}
