﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class TimelineManager : MonoBehaviour
{
    public Camera mainCamera;
    public Camera timelineCamera;
    

    public PlayableDirector timelineAnimation;
    public bool timelineTrigger;

    public Collider thisTriggerCollider;

    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log("魔王TimelineTigger和魔王的距離\t"+Vector3.Distance(this.transform.position, GameObject.Find("Boss").transform.position));
        thisTriggerCollider = GetComponent<Collider>();
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(timelineAnimation.state + timelineAnimation.name);//現在這個Timeline的狀態
        EveryTimelineNoTrigger();

        //測試用的按某個鍵
        if (Input.GetKeyDown(KeyCode.O))
        {
            mainCamera.enabled = false;
            TimelineAnimationStart();
        }

        TimelineAnimationStart();
        
    }

    public void EveryTimelineNoTrigger()
    {
        mainCamera.enabled = true;
        if (timelineCamera!=null)
        {
            if (timelineAnimation.state == PlayState.Paused)
            {
                timelineCamera.enabled = false;
            }
        }
    }

    public void TimelineAnimationStart()
    {
        if (!timelineTrigger) { return; }
        timelineCamera.enabled = true;//切換攝影機
        timelineAnimation.Play();

        

        timelineTrigger = false;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Controller")
        {
            timelineTrigger = true;
            
        }
    }
    
    public void OnTriggerExit(Collider other)
    {        
        timelineTrigger = false;
       //thisTriggerCollider.enabled = false;
    }
}
