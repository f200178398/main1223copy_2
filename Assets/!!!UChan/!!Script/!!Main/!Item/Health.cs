﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SA;
public class Health : MonoBehaviour
{
    public float Repair;
    public bool beRepaired;
    //public GameObject parentPos;
    public GameObject healthPos;
    public float rangeOfArc = 0; //計算用，會一直加
    public float perRangeOfArc = 0.03f; //每秒漂浮變動量
    public float radius = 0.45f;//上下漂浮的範圍

    public float followXPoint = -0.72f;
    public float followYPoint = 2f;
    public float followZPoint = -0.50f;

    public bool settingsHealth;
    public bool droppedHealth;
    //public AI_data data;
    private void Start()
    {
        //healthPos = this.gameObject;
        //data = GetComponent<AI_data>();
    }
    private void Update()
    {
        MovePos();
    }


    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == ("Controller"))
        {
            beRepaired = true;
            Destroy(gameObject, 0.1f);
        }
        
    }
    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == ("Controller"))
        {
           
               beRepaired = false;
            
        }
    }
    public void MovePos()
    {
        rangeOfArc += perRangeOfArc;//一直增加的值
        float dy = Mathf.Cos(rangeOfArc) * radius; //用三角函數讓他那個值在某區間浮動
        if (settingsHealth)
        {
            healthPos.transform.position = new Vector3(transform.position.x, dy + followYPoint, transform.position.z);
        }
        else if (droppedHealth)
        {
            followYPoint = 0f;
            healthPos.transform.position = new Vector3(transform.position.x, this.transform.position.y+dy, transform.position.z);
        }
        
    }
}
