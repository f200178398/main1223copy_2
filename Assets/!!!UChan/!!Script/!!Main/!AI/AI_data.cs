﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Bubble;
namespace SA
{
    public class AI_data : MonoBehaviour
    {
        [Header("=====搜尋各個需要的東西====")]
        [Tooltip("這隻怪獸自己本身")]
        public GameObject enemy;//這個怪獸
        /// <summary>
        /// 這個怪獸的collider
        /// </summary>
        [Tooltip("這隻怪獸自己的Collider")]
        public Collider thisEnemyCollider;
        [Tooltip("射泡泡的腳本")]
        public BubbleShoot_3 bubbleShooter;//這個射擊機
        [Tooltip("玩家的物件")]
        public GameObject controller;
        [Tooltip("EnemyTarget的腳本")]
        public EnemyTarget enemyTarget;
        [Tooltip("FightSystem的腳本")]
        public FightSystem fightSystem;
        [Tooltip("補血道具的物件池")]
        public HealthPool healthPool;
        [Tooltip("怪獸身上的對話視窗上的腳本")]
        public KaiwaWindow kaiwaWindow;
        [Tooltip("雷射打中特效物件池")]
        public LaserHitEffectPool laserHitEffectPool;

        [Header("=====追蹤移動迴避=====")]
        [Tooltip("判斷障礙物的射線長度")] public float rayDistance = 2.0f;
        // Start is called before the first frame update

        [Tooltip("追擊的走路速度")]
        public float Chase_speed = 0.2f;//追擊的速度
        public float kyleSpeed;
        public GameObject target;
        //public float AttackRange = 1.0f;//攻擊範圍
        [HideInInspector] [Tooltip("追擊的走路範圍")] public float ChaseRange = 1.0f;//追擊範圍
        [HideInInspector] public float nextChaseSpeed;
        [Tooltip("視角範圍")]
        public float SightRange = 1.0f;//視角範圍
        [Tooltip("面對玩家的轉動速度")]
        public float Rotationspeed = 0.2f;//轉動速度
        public float kyleRotation;
        [HideInInspector] public Rigidbody rb;
        [Tooltip("往前的速度，向量")]
        public Vector3 movementVec;//往前的速度
        [Tooltip("往前的速度，與玩家之間的向量")]
        public Vector3 dirToPlayer;//npc跟角色的向量
        [Tooltip("是不是在追擊")]
        public bool isChase;
        [Tooltip("下次追擊的計時器")]
        public float float_nextChase;
        [Tooltip("下次追擊的時間設定")]
        public float float_chaseRate;

        [Header("====計時器====")]
        public float fallinBackTime;
        public float fallingBackWaitTime;

        [Header("====後退相關====")]
        [Tooltip("是不是在追擊範圍")]
        public bool inBackRange;
        [Tooltip("是不是在後退狀態")]
        public bool isBack;
        [Tooltip("往後退的走路速度")]
        public float backSpeed;
        [Tooltip("進入往後退狀態的範圍")]
        public float backRang;
        [Tooltip("往後退狀態的計時器")]
        public float backWaitTimer;
        [Tooltip("往後退狀態的時間設定，要等幾秒")]
        public float backWaitTimeSetting;
        [Tooltip("射擊狀態要射多久變走路的計時器")]
        public float shootingToWalkingTimer;
        [Tooltip("射擊狀態要射多久變走路的時間設定")]
        public float shootingToWalkingTimeSetting;
        [Tooltip("攻擊狀態要打多久去走路的計時器")]
        public float attackToWalkingTimer;
        [Tooltip("攻擊狀態要打多久去走路的時間設定")]
        public float attackToWalkingTimeSetting;
        [Tooltip("往後退狀態要退多久的計時器，近戰用")]
        public float backWaitTimerMelle;
        [Tooltip("往後退狀態要退多久的時間設定，近戰用")]
        public float backWaitTimeMelleSetting;
        [HideInInspector] public float backWaitTimerKyle;
        [HideInInspector] public float backWaitTimeKyleSetting;

        [Header("=====血量=====")]
        [Tooltip("怪獸的最大血量")]
        public float maxHp;
        [Tooltip("怪獸現在的血量")]
        public float currentHp;
        [Tooltip("怪獸是不是死了")]
        public bool isDead;
        [Tooltip("怪獸死掉過了多久")]
        public float deathTimer;
        [Header("=====主角的傷害=====")]
        [Tooltip("主角傷害")]
        public float C_Damage;
        [Tooltip("主角打到幾下")]
        public int onTriCount;


        [Header("=====距離判定=====")]
        [HideInInspector] public float float_Distance; //敵我雙方距離
        [Tooltip("敵人的警戒範圍的距離")]
        public float float_checkInSight; //檢查敵人是否進入警戒範圍
        [Tooltip("敵人的緩速區的距離")]
        public float float_SlowDownDistance;
        [HideInInspector] public float float_StopDistance;


        [HideInInspector] public float melleAttackD_Damage;
        [HideInInspector] public float melleAttack_Range;

        //[Header("=====遠程攻擊力=====")]
        [HideInInspector] public float shoot_Damage;

        //[Header("=====攻擊間隔=====")]
        [HideInInspector] public float attackRate;
        [HideInInspector] public float nextAttack;




        #region Move所需要的變數 Move所需要的變數，目前未使用
        [Header("=====各種移動設定=====")]
        public bool bool_EnemyIsMoving;//移動狀態與否   

        public Vector3 vec_EnemyCurrentVector;//怪獸現在的向量
        public float float_EnemyCurrentMoveForce;//怪獸移動的力道
        public float float_EnemyCurrentMoveSpeed;//怪獸現在的移動速度
        public float float_EnemyMaxSpeed;//怪獸的最大移動速度

        [Header("=====各種轉向設定=====")]
        public float float_EnemyMaxRotateSpeed;//怪獸最大的轉向速度
        public float float_EnemyTempRotateForce;//怪獸暫時的轉向力道


        #endregion

        #region 攻擊&射擊所需要的變數
        [Header("=====是否進入攻擊狀態=====")]
        [Tooltip("是不是進入攻擊狀態")]
        public bool bool_Attack;

        [Header("=====各種射擊設定=====")]
        [Tooltip("是否有遠攻屬性")]
        public bool bool_EnemyHasRemoteAttackProperty;//是否具有遠攻屬性
        [HideInInspector] public bool bool_RotateShootProperty;//是否具有旋轉射擊的屬性

        [Space(8)]
        [Tooltip("蜘蛛機器人的炮台")]
        public GameObject go_spiderTurret;//整個砲台
        [Tooltip("蜘蛛機器人砲台上的發射口")]
        public GameObject go_spiderTurretEmitter;//砲台上的發射口
        [Tooltip("蜘蛛機器人砲台的旋轉速度")]
        public float f_spiderTurretRotationSpeed = 1.5f;
        [Tooltip("蜘蛛機器人瞄準角度的補助用向量")]
        public Vector3 v_spiderAimingAngle;
        [Space(8)]
        [Tooltip("雷射蜘蛛機器人的雷射物件")]
        public GameObject go_LaserParent;
        [Tooltip("雷射蜘蛛機器人的雷射啟動光束")]
        public GameObject go_ChargeLaser;
        [Tooltip("雷射蜘蛛機器人的雷射最大威力光束")]
        public GameObject go_BeamLaser;
        [Tooltip("雷射蜘蛛機器人的雷射發射口的煙和粒子物件")]
        public GameObject go_SmokeAndSparks;
        [Tooltip("雷射蜘蛛機器人的雷射結束光束")]
        public GameObject go_EndLaser;
        [HideInInspector]
        public GameObject go_LaserCollider;
        [Tooltip("雷射發射的計時器，Function裡面用\n控制三種雷射啟動→最大→結束用")]
        public float f_LaserTimer;
        [Tooltip("其他狀態要隔多久才可以回到雷射狀態")]
        public float f_LaserRechargeTimeSetting;
        [Tooltip("其他狀態要隔多久才可以回到雷射狀態的計時器")]
        public float f_LaserRechargeTimer;
        [HideInInspector] public bool b_shootVisibleBubble;
        [HideInInspector] public bool b_shootInvisibleBubble;
        [HideInInspector] public bool b_laserColliderOn;
        [Tooltip("發射雷射時，每隔多久進行一次傷害判斷，射出Collider的速度")]
        public float f_LaserColliderShootSpeed;
        [Tooltip("發射雷射時，射出Collider的計時器")]
        public float f_LaserColliderShootTimer;
        //
        [Space(8)]
        [Tooltip("泡泡射擊速度，數字越大越慢(時間是倒數的)")]
        public float float_BubbleShootSpeed;//泡泡射擊速度，數字越大越慢(時間是倒數的)
        [Tooltip("第一次射泡泡的延遲時間")]
        public float float_FirstShootDelayTime;//第一次射泡泡的延遲時間
        [Tooltip("一波泡泡射擊總時間")]
        public float float_WaveShootTotalTime;//一波泡泡射擊總時間
        [Tooltip("泡泡射擊完之後的Delay時間，Delay完之後若條件達成繼續下一波")]
        public float float_WaveShootOverDelayTime;//泡泡射擊完之後的Delay時間，Delay完之後若條件達成繼續下一波
        [Tooltip("泡泡的發射口群組")]
        public Transform[] transArray_Emiiter;//泡泡發射口
        [HideInInspector] public int int_BubbleDamage;//泡泡的傷害

        //
        [HideInInspector] public float float_ShootingDistance;//射擊距離
        [HideInInspector] [Tooltip("砲台瞄準人的旋轉速度")] public float float_RotationSpeed;//砲台瞄準速度

        [Header("=====護盾=====")]
        [Tooltip("護盾的腳本")]
        public Shield defenseShield;
        [Tooltip("有沒有護盾屬性")]
        public bool HaveShield;
        [Tooltip("護盾的物件")]
        public GameObject Shield;
        [Tooltip("護盾已打開")]
        public bool shieldOpen;
        [Tooltip("護盾的半徑")]
        public float shieldRadius;
        [Tooltip("護盾的能量")]
        public float shieldPower;

        [Header("=====各種近戰設定=====")]   // YEN
        public bool bool_EnemyHasMelleAttackProperty; //是否為近戰
        public bool bool_InAttackRange; //是否進入攻擊範圍
        public float float_LookRotationSpeed; //進入某警戒範圍後 會著面朝主角
        public float float_AttackRange; //攻擊範圍
        public float float_FirstAttackCountDown;
        public float float_FirstAttackDelayTime; //遇敵後的第一次攻擊時間
        public float float_MelleAttackRate; //攻擊間隔
        public float float_NextMelleAttack; //下一次的攻擊時間
        [Tooltip("凱爾進入狂暴的血量比例")]
        [Range(0.0f, 0.5f)] public float f_KyleCrazyHealthRate;//凱爾狂暴的血量比例
        public float f_KyleCrazyTimer;
        public float f_KyleCrazyTimeSetting;

        public int int_MelleAttack_Damage;//近戰攻擊傷害
        public string[] MelleAttackAnim; //攻擊的動畫陣列

        public Animator anim;

        [Header("=====承受傷害=====")]
        public bool takeDamage;
        public bool machineDamage;
        public bool bladeFuryDamage;
        public bool isFallingBackDown;
        public bool isStandUp;
        [Header("=====ＴＩＭＥ=====")]
        public bool IsFirstTimeAttack;//是不是第一次攻擊
        public bool IsFirstTimeShoot;
        #endregion

        #region 各種小兵之暴力換狀態變數
        [Header("====各種小兵之暴力換狀態用變數")]
        public int i_KyleStateNumber;
        public int i_LongLegStateNumber;
        public int i_SpiderStateNumber;
        public int i_LaserSpiderStateNumber;

        #endregion

        #region 大魔王所需要的變數

        [Header("====大魔王設定====")]
        public float f_LBMoveSpeed;
        public float f_LBRotationSpeed;
        [HideInInspector] public float f_LBdirToPlayer;//監控用
        public float f_LBTimer;
        [HideInInspector] public float f_LBTimerTemporarySave;//暫時存時間用的，要進入到下個狀態要等幾秒(動畫時間等待用)
        public int i_LBStatePattern;//大魔王的狀態哪一種，進入狀態的條件(為了random)
        [HideInInspector] Vector3 v_LBVectorToPlayer;//向量和上面的追逐共用
        public bool b_LBRotating;//是否在旋轉
        public bool b_LBMoving;//是否在移動
        [HideInInspector] public bool b_LBJumping;
        [HideInInspector] public bool b_LBAttacking;
        [HideInInspector] public Vector3 v_LBVectorChase;//往前追的向量
        public Rigidbody rb_LBrb;
        [HideInInspector] public float f_LBAttackPrepareRange;//大魔王的攻擊距離
        public float f_LBMelleAttackRange1;//跳攻擊和短攻擊的距離，攻擊狀態1
        public float f_LBMelleAttackRange2;//常攻擊距離，攻擊狀態2
        [HideInInspector] public float f_LBDeciedAttackRange;//暫時儲存距離用的變數
        public float f_LBShootAttackRange;//射擊距離
        [HideInInspector] public int i_LBMelleAttackPattern;//random出了哪個攻擊，監看用
        [HideInInspector] public int i_LBShootAttackPattern;//遠攻樣式，監看用
        //public string[] s_LBShootAttackPatternAnimation;
        public bool b_LBEmitterRotate;//射擊時的旋轉口是否旋轉
        [HideInInspector] public float f_LBLeaveStateDistance;//離開狀態時的距離
        public int i_LBHowManyIdleTimesToShootBigBall;//設定幾次的Idle之後一定要射大球

        public float f_LastBossStartActionRange;//大魔王進到大魔王專屬的狀態的距離
        public Vector3 v_LBJumpTeleport;//魔王跳起來瞬間移動的向量
        [HideInInspector] public GameObject go_LBUnderCircleParticle;//魔王腳底下的光圈，跳起來瞬間移動的落下點

        [Header("大魔王發射泡泡的群組")]
        ///
        ///<summary>各emitter的爸爸=emitterholder</summary>
        public GameObject go_LBEmitterHolder;
        public GameObject go_LBAttackEmitterHolder;

        public GameObject[] go_LBJumpShoot;

        public GameObject[] go_LBShootCircle;

        public GameObject[] go_LBShootKome;

        public GameObject[] go_LBShootStar;

        public GameObject[] go_LBShootTriangle;

        public GameObject[] go_LBShootBigBall;

        public GameObject[] go_LBShootBigBall2;

        public GameObject[] go_LBShootAttack1;

        public GameObject[] go_LBShootBigBallTernado;

        public GameObject go_LBJumpShock;

        //下面存成Transform

        ////發射泡泡的各個函式
        //public void LastBossJumpShoot()
        //{
        //    bubbleShooter.MakeBubbleWithPools(tr_LBJumpShoot);
        //}


        #endregion

        #region 好人凱爾所需要的變數
        [Header("====好人凱爾變數設定")]
        public float f_NKTimer;//計時器
        public float f_NKSpeakRange;//講話的範圍
        public float f_NKMoveSpeed;//移動速度
        public float f_NKRotateSpeed;//旋轉速度
        public int i_NKStateNumber;//狀態的編號
        public bool b_NKRotate;//是否允許旋轉
        public Quaternion q_NKStartAngle;//初始角度
        public bool b_NKMove;//可不可以移動
        public GameObject[] otherEnemies;
        public float f_NKAttackRange;//好人凱爾攻擊範圍
        [HideInInspector] public Vector3 v_NKdirToTarget;
        [HideInInspector] public Vector3 v_NKmoveVector;
        public GameObject[] go_NKTargetPosition;
        public Vector3 v_NKAngleToTarget;
        public int i_NKTargetPositionIndex;

        public bool b_talkingKyle;
        public bool b_fightingKyle;
        public float f_DistanceOfAllEnemiesToNiceKyle;
        public bool b_enemiesInNiceKyleAttackRange;
        public GameObject go_nearestEnemyToNiceKyle;
        public float f_DistanceOfNearstEnemyToNiceKyle;
        //

        #endregion

        #region 壞人凱爾打好人凱爾
        public GameObject[] niceKyles;
        public GameObject go_nearestNiceKyleToBadKyle;
        public bool b_badKyleFindNiceKyle;
        public float f_DistanceOfNiceKyleToBadKyle;
        public float f_DistanceOfNearestNiceKyleToBadKyle;
        public bool b_BadKyleHitByPlayer;
        #endregion

        #region 雷射蜘蛛機器人的射線
        public LineRenderer lr_laserSpiderLaser;
        public Ray r_laserSpiderLaser;
        public LaserColliderPool laserColliderPool;
        public LaserMaker.LaserHitGroundEffectPool laserHitGroundEffectPool;
        public float f_laserHitGroundMakeEffectTime=0.25f;
        #endregion

        #region 凱爾牆
        public int i_KyleWallStateNumber;
        #endregion

        #region 魔王資料
        public AI_data boss_data;
        public EnemyTarget boss_target;
        #endregion 

        public void OnDrawGizmos()
        {
            Vector3 thisEnemyPosition = this.transform.position;
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(thisEnemyPosition, float_AttackRange);//劃出進戰攻擊範圍

            //Gizmos.color = Color.blue;
            //Gizmos.DrawWireSphere(thisEnemyPosition, float_checkInSight);//劃出進戰攻擊範圍

            //Gizmos.color = Color.blue;
            //Gizmos.DrawWireSphere(thisEnemyPosition, float_SlowDownDistance);

            //Gizmos.color = Color.red;
            //Gizmos.DrawWireSphere(this.transform.position, AttackRange);//攻擊範圍
            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(this.transform.position, this.transform.position + this.transform.forward * rayDistance);

            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(thisEnemyPosition, backRang);

            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);//警戒範圍

            Gizmos.color = Color.magenta;
            //Gizmos.DrawWireSphere(this.transform.position, ChaseRange);//追擊範圍
            //Vector3 g1Rector = Quaternion.AngleAxis(-45f, Vector3.up) * transform.forward;

            //Vector3 g2Rector = Quaternion.AngleAxis(-22.5f, Vector3.up) * transform.forward;
            //Vector3 g3Rector = Quaternion.AngleAxis(22.5f, Vector3.up) * transform.forward;
            //Vector3 g4Rector = Quaternion.AngleAxis(45f, Vector3.up) * transform.forward;
            ////旁邊兩條

            //Gizmos.color = Color.magenta;
            //Gizmos.DrawLine(this.transform.position, this.transform.position + g1Rector * SightRange);
            //Gizmos.DrawLine(this.transform.position, this.transform.position + g4Rector * SightRange);
            ////
            //Gizmos.DrawLine(this.transform.position + g1Rector * SightRange, this.transform.position + g2Rector * SightRange);
            //Gizmos.DrawLine(this.transform.position + g2Rector * SightRange, this.transform.position + g3Rector * SightRange);
            //Gizmos.DrawLine(this.transform.position + g3Rector * SightRange, this.transform.position + g4Rector * SightRange);
            LastBossGizmos();
        }
        #region 魔王的線
        private void LastBossGizmos()
        {


            //Gizmos.color = Color.cyan;
            //Gizmos.DrawLine(this.transform.position, this.transform.position + this.transform.forward * 5.0f);

            //Gizmos.color = Color.white;
            //Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);

            if (enemyTarget == null)
            {
                return;
            }

            if (enemyTarget.currentState == StateType.Idle)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);
            }
            else if (enemyTarget.currentState == StateType.LastBossFirstIdle)
            {
                Gizmos.color = new Color(0, 0.8f, 0);
                Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);
            }
            else if (enemyTarget.currentState == StateType.LastBossFirstShoot)
            {
                Gizmos.color = Color.magenta;
                Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);
            }
            else if (enemyTarget.currentState == StateType.LastBossChase1)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);
            }
            else if (enemyTarget.currentState == StateType.LastBossJump1)
            {
                Gizmos.color = new Color(1.0f, 0.5f, 0);
                Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);
            }
            else if (enemyTarget.currentState == StateType.LastBossAttackNew1)
            {
                Gizmos.color = new Color(1.0f, 0, 0);
                Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);
            }
            else if (enemyTarget.currentState == StateType.LastBossShootBigBall)
            {
                Gizmos.color = new Color(1.0f, 0.6f, 0.6f);
                Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);
            }
            else if (enemyTarget.currentState == StateType.LastBossDeath)
            {
                Gizmos.color = Color.gray;
                Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);
            }
        }
        #endregion








        private void Awake()
        {

            //AI_data一開始找東西的時候，先找自己，把該要的參考資料都找到
            enemy = this.gameObject;
            thisEnemyCollider = this.GetComponent<Collider>();
            bubbleShooter = enemy.GetComponentInChildren<BubbleShoot_3>();
            fightSystem = GameObject.Find("FightSystem").GetComponent<FightSystem>();
            anim = GetComponent<Animator>();
            controller = GameObject.Find("Controller");
            currentHp = maxHp;
            rb = GetComponent<Rigidbody>();
            target = GameObject.Find("Controller");
            defenseShield = enemy.GetComponentInChildren<Shield>();
            rb_LBrb = GetComponent<Rigidbody>();
            FindLastBossEmitters();//自動找發射點的函式，大魔王用
            healthPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<HealthPool>();
            enemyTarget = GetComponent<EnemyTarget>();

            //好人凱爾一開始的角度
            q_NKStartAngle = enemy.transform.rotation;
            otherEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            niceKyles = GameObject.FindGameObjectsWithTag("NiceKyle");

            //雷射射中特效物件池
            laserHitEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<LaserHitEffectPool>();
            laserColliderPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<LaserColliderPool>();

            //每個人都取得魔王身上的資料
            boss_target = GameObject.Find("Boss").GetComponent<EnemyTarget>();
        }


        private void Start()
        {
            NiceKyleFindKaiwaWindow();
            if (enemyTarget.b_IsKyle)
            {
                Chase_speed = kyleSpeed;

                Rotationspeed = kyleRotation;
            }
        }



        // Update is called once per frame
        void Update()
        {


        }


        public void OnTriggerEnter(Collider other) //只能在第一個CLASS觸發
        {
            //凱爾牆的傷害判斷
            if (enemyTarget.b_IsKyleWall)
            {
                if (other.gameObject.tag == "Weapon_point")
                {
                    onTriCount++;
                    fightSystem.attack = true;
                    print("命中敵人");
                    takeDamage = true;
                    currentHp -= fightSystem.UpGradeDamage();
                    anim.SetTrigger("take damageTri");
                    anim.SetTrigger("UndamageTri");
                }
                if (other.gameObject.tag == "Bullet")
                {
                    machineDamage = true;
                    currentHp -= fightSystem.machineDamage;
                }
                if (other.gameObject.tag == "warpCol")
                {
                    currentHp -= fightSystem.UpGradeDamage() * 2f;
                }
                if (other.gameObject.tag == "BladeFury")
                {
                    currentHp -= 200f;
                }
            }

            if (other.gameObject.tag == "Weapon_point") /// 必須將主角武器設為"Weapon_point" !! 避免其他碰撞體傷害敵人
            {
                onTriCount++;
                fightSystem.attack = true;
                print("命中敵人");
                takeDamage = true;
                currentHp -= fightSystem.UpGradeDamage();
                anim.SetTrigger("take damageTri");

                //print("敵人當前血量 : " + currentHp);

                //壞人凱爾被主角打到了→強制從打好人凱爾狀態轉成Chase；Chase是不會進入到打凱爾狀態的，但是Idle會，所以進入打好凱爾狀態的條件是 bool=false和找到範圍內有好凱爾
                b_BadKyleHitByPlayer = true;
            }

            if (other.gameObject.tag == "warpCol")
            {
                currentHp -= fightSystem.UpGradeDamage() * 2f;

                print("被閃現攻擊");

                //壞人凱爾被主角打到了→強制從打好人凱爾狀態轉成Chase；Chase是不會進入到打凱爾狀態的，但是Idle會，所以進入打好凱爾狀態的條件是 bool=false和找到範圍內有好凱爾
                b_BadKyleHitByPlayer = true;
            }
            //else
            //{
            //    //fightSystem.attack = false;
            //    anim.SetTrigger("UndamageTri");
            //    takeDamage = false;
            //}

            //bug : 射擊後  和武器一同造成傷害


            if (other.gameObject.tag == "Bullet") /// BUG: 會一直造成損傷 就算沒有射擊  
            {
                machineDamage = true;
                currentHp -= fightSystem.machineDamage;
                //anim.SetTrigger("take damageTri");

                print("machineDamage");
                //takeDamege = false;

                //壞人凱爾被主角打到了→強制從打好人凱爾狀態轉成Chase；Chase是不會進入到打凱爾狀態的，但是Idle會，所以進入打好凱爾狀態的條件是 bool=false和找到範圍內有好凱爾
                b_BadKyleHitByPlayer = true;
            }
            else
            {
                //anim.SetTrigger("UndamageTri");
                machineDamage = false;
            }

            if (other.gameObject.tag == "BladeFury")
            {
                currentHp -= 200f;
                if (currentHp > 0)
                {
                    /*anim.SetTrigger("falling");*/ //播放被擊倒的動畫
                    bladeFuryDamage = true;
                    isFallingBackDown = true;
                }
                //anim.SetTrigger("take damageTri");    


                //壞人凱爾被主角打到了→強制從打好人凱爾狀態轉成Chase；Chase是不會進入到打凱爾狀態的，但是Idle會，所以進入打好凱爾狀態的條件是 bool=false和找到範圍內有好凱爾
                b_BadKyleHitByPlayer = true;
            }

            if (other.gameObject.tag == "PowerChop")
            {
                currentHp -= 1000f;
                bladeFuryDamage = true;
                isFallingBackDown = true;
                anim.SetTrigger("take damageTri");


                //壞人凱爾被主角打到了→強制從打好人凱爾狀態轉成Chase；Chase是不會進入到打凱爾狀態的，但是Idle會，所以進入打好凱爾狀態的條件是 bool=false和找到範圍內有好凱爾
                b_BadKyleHitByPlayer = true;
            }

            //好人凱爾被打
            if (other.gameObject.tag == "Enemy_Attack_BOX" /*&& enemyTarget.b_IsNiceKyle*/ )
            {
                //Debug.Log("Hit");
                anim.SetTrigger("BeHit");
            }

            if(other.gameObject.tag == "Laser")
            {
                currentHp -= 800f;
                anim.SetTrigger("take damageTri");
            }
            if (other.gameObject.tag == "Bomb")
            {
                currentHp -= 1200f;
                anim.SetTrigger("take damageTri");
            }
        }
        public void OnTriggerExit(Collider other) //  未 執行攻擊時 將weaponTrigger的attackTri 給關閉 //似乎邏輯不通?
        {
            if (other.gameObject.tag == "Weapon_point")
            {
                fightSystem.attack = false;
                takeDamage = false;
                machineDamage = false;
            }


        }

        public void OpenEnemyCol()
        {
            thisEnemyCollider.enabled = enabled;
        }

        public void CloseEnemyCol()
        {
            thisEnemyCollider.enabled = !enabled;
        }
        public float GetHealthRate()
        {
            return currentHp / maxHp;
        }
        //int IComparable<AI_data>.CompareTo(AI_data other)   // 使用IComparable 時必須加上去
        //{
        //    throw new NotImplementedException();
        //}

        public void FindLastBossEmitters()
        {
            #region 大魔王的發射點群組
            go_LBJumpShoot = GameObject.FindGameObjectsWithTag("JumpEmitter");
            go_LBShootBigBall = GameObject.FindGameObjectsWithTag("BigBallEmitter");
            go_LBShootCircle = GameObject.FindGameObjectsWithTag("CircleEmitter");
            go_LBShootKome = GameObject.FindGameObjectsWithTag("KomeEmitter");
            go_LBShootStar = GameObject.FindGameObjectsWithTag("StarEmitter");
            go_LBShootTriangle = GameObject.FindGameObjectsWithTag("TriangleEmitter");
            go_LBShootBigBallTernado = GameObject.FindGameObjectsWithTag("TernadoEmitter");
            go_LBShootAttack1 = GameObject.FindGameObjectsWithTag("ShootAttack1Emitter");
            go_LBEmitterHolder = GameObject.FindGameObjectWithTag("LB_EmitterHolder");

            go_LBAttackEmitterHolder = GameObject.FindGameObjectWithTag("LB_AttackEmitterHolder");
            go_LBShootBigBall2 = GameObject.FindGameObjectsWithTag("BigBallEmitter2");

            #endregion
        }

        public void NiceKyleFindKaiwaWindow()
        {
            if (enemyTarget.b_IsNiceKyle == false || enemyTarget.b_IsKyle == false)
            {
                return;
            }


            kaiwaWindow = GameObject.FindGameObjectWithTag("Kaiwa").GetComponent<KaiwaWindow>();

        }

        #region 壞人凱爾狂暴狀態掛在動畫上的Function
        public void KyleCrazyKaiwaWindowOpen1()
        {
            kaiwaWindow.b_controllerInRange = true;
            kaiwaWindow.ShowMessage(kaiwaWindow.go_kaiwaWindow1);
        }
        public void KyleCrazyKaiwaWindowOpen2()
        {
            kaiwaWindow.b_controllerInRange = true;
            kaiwaWindow.ShowMessage(kaiwaWindow.go_kaiwaWindow2);
        }
        public void KyleCrazyKaiwaWindowClose1()
        {
            kaiwaWindow.b_controllerInRange = false;
            kaiwaWindow.ShowMessage(kaiwaWindow.go_kaiwaWindow1);
        }
        public void KyleCrazyKaiwaWindowClose2()
        {
            kaiwaWindow.b_controllerInRange = false;
            kaiwaWindow.ShowMessage(kaiwaWindow.go_kaiwaWindow2);
        }
        public void KyleCrazyMoveSpeedOpen()
        {
            Chase_speed = 0.08f;
        }
        public void KyleCrazyMoveSpeedClose()
        {
            Chase_speed = 0f;
        }
        #endregion

        #region 魔王掛在動畫上的Function
        public void LastBossMakeJumpShock()
        {
            GameObject jumpClone = Instantiate(go_LBJumpShock, this.transform.position, this.transform.rotation);
        }
        public void LastBossJumpTeleport()
        {

            enemy.transform.position = Vector3.Slerp(enemy.transform.position, enemy.transform.position + v_LBJumpTeleport, 1.0f);
        }
        //public void LastBossJumpTeleportCircleParticleOpen()
        //{
        //    lastBossJumpTeleportCirclePool.ReUse(enemy.transform.position + v_LBJumpTeleport, enemy.transform.rotation);
        //}
        public void LastBossJumpTeleportCircleParticleClose()
        {
            go_LBUnderCircleParticle.SetActive(false);
        }
        public void LastBossTeleportCircleMove()
        {

        }
        public void LastBossJumpSpeedOpen()
        {
            b_LBMoving = true;
            f_LBMoveSpeed = UnityEngine.Random.Range(0.3f, 0.5f);
        }
        public void LastBossJumpSpeedClose()
        {
            b_LBMoving = false;
            f_LBMoveSpeed = 0.0f;
        }
        #endregion
    }



}







