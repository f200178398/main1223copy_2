﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SA;
public class AI_Function : MonoBehaviour
{

    private void Start()
    {

    }

    #region 目前沒用到的Function

    /// <summary>
    /// 抄老師的Move，目前沒有在使用
    /// </summary>
    /// <param name="data"></param>
    public static void Move(AI_data data)
    {
        if (data.bool_EnemyIsMoving == false)//如果移動狀態=假，不做這個Move的動作
        {
            return;
        }

        Transform thisEnemyTransfrom = data.enemy.transform;//這隻怪獸的位置一開始的Trasform，先存起來
        Vector3 thisEnemyPosition = data.enemy.transform.position;//這怪獸的位置，先存起來
        Vector3 thisEnemyRight = thisEnemyTransfrom.right;//這怪獸右方
        Vector3 thisEnemyForward = thisEnemyTransfrom.forward;//這怪獸前方，用來偵測障礙物用的，什麼都沒有變過的前方先存起來
        Vector3 thisEnemyAddForceForMove = data.vec_EnemyCurrentVector;//這怪獸要移動的力量

        //下面這一串讓轉向力量太大的話不超過怪獸轉向最大速度
        if (data.float_EnemyTempRotateForce > data.float_EnemyMaxRotateSpeed)
        {
            data.float_EnemyTempRotateForce = data.float_EnemyMaxRotateSpeed;
        }
        else if (data.float_EnemyTempRotateForce < -data.float_EnemyMaxRotateSpeed)
        {
            data.float_EnemyTempRotateForce = -data.float_EnemyMaxRotateSpeed;
        }

        //下面這一串才是移動，先算出要往哪邊走
        thisEnemyAddForceForMove = thisEnemyAddForceForMove + thisEnemyRight * data.float_EnemyTempRotateForce;//總力道，包刮往前和轉彎
        thisEnemyAddForceForMove.Normalize();//把力道Normolize
        thisEnemyTransfrom.forward = thisEnemyAddForceForMove;//把移動的力量指派給往前的向量

        //下面這一串決定移動的速度
        data.float_EnemyCurrentMoveSpeed = data.float_EnemyCurrentMoveSpeed + data.float_EnemyCurrentMoveForce * Time.deltaTime;//速度=力道*Time
        if (data.float_EnemyCurrentMoveSpeed < 0.01f)//如果速度太小的話，讓他還是會移動，不要太小小到一直在計算
        {
            data.float_EnemyCurrentMoveSpeed = 0.01f;
        }
        else if (data.float_EnemyCurrentMoveSpeed > data.float_EnemyMaxSpeed)//如果超過怪獸最大速度，讓他等於最大速不要超過的意思
        {
            data.float_EnemyCurrentMoveSpeed = data.float_EnemyMaxSpeed;
        }

        //check障礙物先跳過

        //下面是算出位置(終於要移動的意思)
        thisEnemyPosition = thisEnemyPosition + thisEnemyTransfrom.forward * data.float_EnemyCurrentMoveSpeed;//位移=現在位置+往前的向量(已存Normalized後的力)
        thisEnemyTransfrom.position = thisEnemyPosition;//把新的位置告訴怪獸的Transform
    }

    /// <summary>
    /// 確認玩家是否在視距內，目前未使用到
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static bool CheckInSight(AI_data data) //檢查是否視距內
    {
        GameObject enemy = data.enemy;
        RaycastHit hit;
        Vector3 v = data.dirToPlayer;
        GameObject controller = data.controller;
        data.dirToPlayer = new Vector3((controller.transform.position.x - enemy.transform.position.x), 0.0f, (controller.transform.position.z - enemy.transform.position.z)).normalized;

        float angle = Mathf.Atan2(data.dirToPlayer.x, data.dirToPlayer.z);
        float distance = Vector3.Distance(enemy.transform.position, controller.transform.position);
        bool insight = true;
        if (distance < data.float_checkInSight)
        {

            if (Physics.Raycast(enemy.transform.position, enemy.transform.forward, out hit, data.rayDistance) && (hit.collider.gameObject.tag != "Controller" + "Scene_Street "))//撞到牆要迴避
            {

                Debug.DrawLine(enemy.transform.position, hit.point, Color.red);
                print(enemy.name + "碰撞物:" + hit.collider.gameObject.name);
                data.dirToPlayer += hit.normal * 30.0f;

            }
            else
            {
                // dirToPlayer = new Vector3((target.transform.position.x - this.transform.position.x), 0.0f, (target.transform.position.z - this.transform.position.z)).normalized;
                Debug.DrawRay(enemy.transform.position, enemy.transform.forward * data.rayDistance, Color.yellow);

                //dirToPlayer = dirToPlayer;
            }

            //Quaternion lookRotation = Quaternion.LookRotation(data.dirToPlayer, Vector3.up);
            //enemy.transform.rotation = Quaternion.Slerp(enemy.transform.rotation, lookRotation, Time.deltaTime * data.Rotationspeed);
            //data.movementVec = enemy.transform.forward * data.Chease_speed;

            //data.Chease_speed = 0.2f;
            //data.anim.SetBool("chase", true);
            //data.rb.MovePosition(data.rb.transform.position + data.movementVec * 1f);
            //data.isChase = true;
        }

        //bool insight;
        ////print(Distance(data));
        //if (Distance(data) < data.float_checkInSight)
        //{

        //    insight = true;
        //    print(data.enemy.name + "CheckInSight :"  +insight);
        //    //Chase(data);
        //    return insight;
        //}
        //else
        //{
        //    insight = false;
        //    return insight;
        //}
        return insight;
    }

    /// <summary>
    /// 檢查是否進入後退範圍，本來用於小兵狀態遷移，目前不使用
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static bool InBackRange(AI_data data)
    {

        if (Distance(data) > data.backRang)//再後退範圍內
        {

            data.inBackRange = false;
        }
        else
        {
            data.inBackRange = true;
        }
        return data.inBackRange;
    }

    /// <summary>
    /// 不後退的函式，目前不使用
    /// </summary>
    /// <param name="data"></param>
    public static void UnBack(AI_data data)
    {
        //if(Distance(data) > data.backRang)

        //{
        if (data.inBackRange == false)
        {
            data.backWaitTimer = 0;
            print("不後退!!!!!!!!!!!!!!");

            //data.backSpeed = 0;
            data.anim.SetBool("Back", false);
            data.isBack = false;
        }
        //} 
    }

    /// <summary>
    /// 後退的動作，測使版，目前不使用
    /// </summary>
    /// <param name="data"></param>
    public static void Back(AI_data data)
    {
        GameObject controller = data.controller;
        GameObject enemy = data.enemy;
        float distance = Vector3.Distance(data.enemy.transform.position, data.controller.transform.position);
        data.dirToPlayer = new Vector3((controller.transform.position.x - enemy.transform.position.x), 0.0f, (controller.transform.position.z - enemy.transform.position.z)).normalized;
        if (distance < data.backRang)//再後退範圍內
        {
            data.backWaitTimer += Time.deltaTime;
            //data.Chease_speed = -0.05f;

            //data.anim.SetBool("Back", true);
            data.isChase = false;

            if (data.backWaitTimer > data.backWaitTimeSetting && distance < data.backRang)   //在主角進入後退範圍後的一小段後, 執行後退
            {
                data.anim.SetBool("Back", true);
                data.isChase = false;
                data.backSpeed = -0.02f;


                data.bool_InAttackRange = false;

                data.backWaitTimer = 0;
            }
            Quaternion lookRotation = Quaternion.LookRotation(data.dirToPlayer, Vector3.up);
            enemy.transform.rotation = Quaternion.Slerp(enemy.transform.rotation, lookRotation, Time.deltaTime * data.Rotationspeed);
            //data.movementVec = enemy.transform.forward * data.backSpeed;

            data.movementVec = enemy.transform.forward * -1f;
            data.anim.SetBool("Back", true);
            data.rb.MovePosition(data.rb.transform.position + data.movementVec * 0.1f);
            data.isBack = true;
        }


    }

    /// <summary>
    /// 抄老師的Chase，目前不使用
    /// </summary>
    /// <param name="data"></param>
    public static void Chase(AI_data data) //備用
    {
        GameObject enemy = data.enemy;
        RaycastHit hit;
        Vector3 v = data.dirToPlayer;
        GameObject controller = data.controller;
        data.dirToPlayer = new Vector3((controller.transform.position.x - enemy.transform.position.x), 0.0f, (controller.transform.position.z - enemy.transform.position.z)).normalized;

        float angle = Mathf.Atan2(data.dirToPlayer.x, data.dirToPlayer.z);
        float distance = Vector3.Distance(enemy.transform.position, controller.transform.position);
        if (!Dead(data))
        {
            if (distance > data.float_checkInSight)
            {
                data.anim.SetBool("chase", false);
                data.isChase = false;
            }

            if (distance < data.float_checkInSight)
            {

                if (Physics.Raycast(enemy.transform.position, enemy.transform.forward, out hit, data.rayDistance) && (hit.collider.gameObject.tag != "Controller" + "Scene_Street "))//撞到牆要迴避
                {

                    Debug.DrawLine(enemy.transform.position, hit.point, Color.red);
                    print(enemy.name + "碰撞物:" + hit.collider.gameObject.name);
                    data.dirToPlayer += hit.normal * 30.0f;

                }
                else
                {
                    // dirToPlayer = new Vector3((target.transform.position.x - this.transform.position.x), 0.0f, (target.transform.position.z - this.transform.position.z)).normalized;
                    Debug.DrawRay(enemy.transform.position, enemy.transform.forward * data.rayDistance, Color.yellow);
                    //dirToPlayer = dirToPlayer;
                }

                Quaternion lookRotation = Quaternion.LookRotation(data.dirToPlayer, Vector3.up);
                enemy.transform.rotation = Quaternion.Slerp(enemy.transform.rotation, lookRotation, Time.deltaTime * data.Rotationspeed);
                data.movementVec = enemy.transform.forward * data.Chase_speed;

                float speed = data.Chase_speed;
                data.anim.SetBool("chase", true);
                data.rb.MovePosition(data.rb.transform.position + data.movementVec * 1f);
                data.isChase = true;
            }

            if (distance > data.float_AttackRange && distance <= data.float_SlowDownDistance)
            {

                Quaternion lookRotation = Quaternion.LookRotation(data.dirToPlayer, Vector3.up);
                enemy.transform.rotation = Quaternion.Slerp(enemy.transform.rotation, lookRotation, Time.deltaTime * data.Rotationspeed);
                data.movementVec = enemy.transform.forward * data.Chase_speed;


            }

            if (distance < data.float_AttackRange)  // 當距離小於攻擊範圍 速度為0  開始攻擊 並且 當離開攻擊範圍後  開始計時 並且兩秒後 才開始追擊
            {
                data.Chase_speed = 0f;
                data.anim.SetBool("chase", false);
                data.bool_InAttackRange = true;
                data.isChase = false;

            }
            else if (distance > data.float_AttackRange && data.bool_InAttackRange == true)
            {
                data.float_nextChase += Time.deltaTime;
                //print(" 超出攻擊範圍");
                data.anim.SetBool("chase", false);
                data.Chase_speed = 0f;
                if (data.float_nextChase > data.float_chaseRate && distance > data.float_AttackRange)   //會在主角離開攻擊範圍後數秒再次追擊
                {
                    data.anim.SetBool("chase", true);
                    data.isChase = true;
                    data.Chase_speed = 0.1f;


                    data.bool_InAttackRange = false;

                    data.float_nextChase = 0;
                }


            }

        }
    }

    /// <summary>
    /// 開啟護盾
    /// </summary>
    /// <param name="data"></param>
    public static void OpenShield(AI_data data) ///防護盾用
    {

        GameObject Shield = data.Shield;
        GameObject enemy = data.enemy;
        if (data.HaveShield == true)
        {


            if (Distance(data) <= data.shieldRadius)//小於攻擊距離
            {

                if (enemy.GetComponent<AI_data>().shieldPower == 0)//防護盾被打爆==0時
                {
                    data.shieldOpen = false;
                    Shield.gameObject.SetActive(false);
                    //  Box.transform.localScale =Box.transform.localScale+new Vector3(Time.deltaTime,1,1);
                    enemy.GetComponent<Rigidbody>().isKinematic = true;
                    // print("泡泡破掉了");
                }

                else//沒被打爆靠近防護盾，防護盾起作用
                {
                    data.shieldOpen = true;
                    //print("start shield");
                    Shield.gameObject.SetActive(true);
                    Shield.transform.localScale = Shield.transform.localScale + new Vector3(1f, 1f, 1f);

                    enemy.GetComponent<Rigidbody>().isKinematic = true;
                }

            }

        }
    }

    /// <summary>
    /// 關閉護盾
    /// </summary>
    /// <param name="data"></param>
    public static void ColseShield(AI_data data)
    {
        GameObject Shield = data.Shield;
        GameObject enemy = data.enemy;
        if (data.HaveShield == true)
        {
            if (Distance(data) > data.float_AttackRange)//當主角跑離怪的偵查範圍
            {
                Shield.transform.localScale = Shield.transform.localScale - new Vector3(0.2f, 0.2f, 0.2f); //防護盾慢慢變小
                if (Shield.transform.localScale.x < 0)//防護盾不小於0縮放值
                {
                    Shield.transform.localScale = new Vector3(0, 0, 0);
                }
                //Box.gameObject.SetActive(false);
                enemy.GetComponent<Rigidbody>().isKinematic = false;

            }
        }
    }

    /// <summary>
    /// 看著主角，未完成
    /// </summary>
    /// <param name="data"></param>
    public static void LookAtController(AI_data data) //未完成
    {

        Vector3 v = data.controller.transform.position - data.enemy.transform.position;
        float fDist = v.magnitude;
        if (fDist < data.float_checkInSight && !Dead(data))
        {
            float _lookRange = data.float_checkInSight + 5f;
            Quaternion rotation = Quaternion.LookRotation(data.controller.transform.position - data.enemy.transform.position, Vector3.up);
            data.enemy.transform.rotation = Quaternion.Slerp(data.enemy.transform.rotation, rotation, Time.deltaTime * data.float_LookRotationSpeed);

        }


    }

    /// <summary>
    /// 第一次攻擊的倒數，目前未使用
    /// </summary>
    /// <param name="data"></param>
    public static void FirstTimeShoot(AI_data data)

    {
        if (data.IsFirstTimeShoot)
        {
            data.bubbleShooter.firstShootingCountDown = data.float_FirstShootDelayTime;
            data.IsFirstTimeShoot = false;
        }
        else
        {
            data.bubbleShooter.firstShootingCountDown -= Time.deltaTime;
            //Debug.Log("First Attack Preparing" + data.bubbleShooter.firstShootingCountDown);
            if (data.bubbleShooter.firstShootingCountDown < 0)
            {


                return;
                data.bubbleShooter.firstShootingCountDown = 0;
                print(data.bubbleShooter.firstShootingCountDown);
            }
        }

    }

    //public static bool NextTimeChase(AI_data data ,ref bool _startChase)
    //{

    //    data.float_nextChase += Time.deltaTime;
    //    if (data.float_nextChase > data.float_chaseRate)
    //    {
    //        data.float_nextChase = 0;
    //        return true;
    //    }
    //    else
    //    {

    //        return false;
    //    }
    //}



    //public void FirstTimeShoot(AI_States data)
    //{
    //    if (data.)
    //    {
    //        data.bubbleShooter.firstShootingCountDown = data.float_FirstShootDelayTime;
    //        data.isfi = false;
    //    }
    //    else
    //    {
    //        data.bubbleShooter.firstShootingCountDown -= Time.deltaTime;
    //        Debug.Log("First Attack Preparing" + data.bubbleShooter.firstShootingCountDown);
    //    }
    //    if (data.bubbleShooter.firstShootingCountDown < 0)
    //    {
    //        data.bubbleShooter.firstShootingCountDown = 0;
    //        print("rest");
    //    }

    //}

    /// <summary>
    /// 砲台瞄準，目前沒使用
    /// </summary>
    /// <param name="data"></param>
    public static void TurrentRotation(AI_data data)
    {
        Quaternion rotation = Quaternion.LookRotation(data.controller.transform.position - data.enemy.transform.position, Vector3.up);
        data.enemy.transform.rotation = Quaternion.Slerp(data.enemy.transform.rotation, rotation, Time.deltaTime * data.float_RotationSpeed);
    }

    #endregion

    #region 小兵們用的

    /// <summary>
    /// 確認主角是否進入視野範圍，回傳一個GameObject
    /// </summary>
    /// <param name="data"></param>
    /// <param name="bAttack"></param>
    /// <returns></returns>
    public static GameObject CheckControllerInSight(AI_data data, ref bool bAttack)
    {
        GameObject controller = data.controller;
        Vector3 v = controller.transform.position - data.enemy.transform.position;
        float fDist = v.magnitude; //距離為向量長度
        if (fDist < data.float_AttackRange) //距離小於攻擊範圍時
        {
            //Debug.Log("進入敵人 :" + data.enemy.name + " 的攻擊範圍");
            bAttack = true;
            Debug.Log("進入敵人 :" + data.enemy.name + " 的攻擊範圍" + bAttack);
            return controller;
        }
        else if (fDist < data.float_checkInSight)
        {
            Debug.Log("進入敵人 :" + data.enemy.name + " 的警戒範圍" + bAttack);
            bAttack = false;
            return controller;
        }
        return null;
    }

    /// <summary>
    /// 確認主角是否進入範圍內，Bool值，Idle狀態遷移至Chase狀態的判斷基準
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static bool CheckTargetEnemyInSight(AI_data data)
    {
        GameObject enemy = data.enemy;
        Vector3 v = enemy.transform.position - data.controller.transform.position;
        float fDist = v.magnitude;
        if (fDist < data.float_AttackRange)
        {
            data.bool_Attack = true;
            print("敵人: " + enemy.name + "的攻擊狀態 : " + data.bool_Attack);
            return true;
        }
        else if (fDist < data.float_checkInSight)
        {
            data.bool_Attack = false;
            return true;
        }
        return false;
    }

    /// <summary>
    /// 射泡泡，蜘蛛機器人用
    /// </summary>
    /// <param name="data"></param>
    public static void Shoot(AI_data data)
    {
        //是否具有遠距攻擊屬性，沒的話就掰
        if (data.bool_EnemyHasRemoteAttackProperty == false)
        {
            data.transArray_Emiiter = null;//如果沒有射擊屬性，發射口也給空
            return;
        }

        //把AI_data的泡泡射擊速度 傳到 BubbleShoot_3 裡，再把它存起來
        data.bubbleShooter.resetShootingTime = data.float_BubbleShootSpeed;
        float f_BubbleShootSpeed = data.bubbleShooter.resetShootingTime;
        //同上，第一次射擊前的延遲時間
        data.bubbleShooter.firstShootingCountDown = data.float_FirstShootDelayTime;
        float f_FirstShootDelayTime = data.bubbleShooter.firstShootingCountDown;
        //同上，一波泡泡攻擊時間
        data.bubbleShooter.totalWaveShootingTime = data.float_WaveShootTotalTime;
        float f_WaveShootTotalTime = data.bubbleShooter.totalWaveShootingTime;
        //同上，整波的射完之後的延遲時間
        data.bubbleShooter.rechargeTime = data.float_WaveShootOverDelayTime;
        float f_WaveShootOverDelayTime = data.bubbleShooter.rechargeTime;
        //同上，泡泡的發射口(注意是一個陣列)
        data.bubbleShooter.bubbleEmitter = data.transArray_Emiiter;
        Transform[] trans_array_Emitter = data.bubbleShooter.bubbleEmitter;

        data.anim.SetBool("chase", false); //射擊時候CHASE 試試看能否寫在狀態機

        //Debug.Log("AIFunction");

        data.bubbleShooter.BubbleShooting();// 開啟後開始發射



    }

    /// <summary>
    /// 雷射射出一個隱形的圓柱體，本來是要射出一堆隱形泡泡造成傷害，現在射一根不會動的
    /// </summary>
    /// <param name="data"></param>
    public static void ShootLaserCollider(AI_data data)
    {
        if (data.bool_EnemyHasRemoteAttackProperty == false)
        {
            data.transArray_Emiiter = null;//如果沒有射擊屬性，發射口也給空
            return;
        }
        //用bool控制棒子可以製造幾根，在狀態裡面有控制
        //if ( !data.b_laserColliderOn)
        //{
        //    return;
        //}



        ////把AI_data的泡泡射擊速度 傳到 BubbleShoot_3 裡，再把它存起來
        //data.bubbleShooter.resetShootingTime = data.float_BubbleShootSpeed;
        //float f_BubbleShootSpeed = data.bubbleShooter.resetShootingTime;
        ////同上，第一次射擊前的延遲時間
        //data.bubbleShooter.firstShootingCountDown = data.float_FirstShootDelayTime;
        //float f_FirstShootDelayTime = data.bubbleShooter.firstShootingCountDown;
        ////同上，一波泡泡攻擊時間
        //data.bubbleShooter.totalWaveShootingTime = data.float_WaveShootTotalTime;
        //float f_WaveShootTotalTime = data.bubbleShooter.totalWaveShootingTime;
        ////同上，整波的射完之後的延遲時間
        //data.bubbleShooter.rechargeTime = data.float_WaveShootOverDelayTime;
        //float f_WaveShootOverDelayTime = data.bubbleShooter.rechargeTime;
        //同上，泡泡的發射口(注意是一個陣列)
        data.bubbleShooter.bubbleEmitter = data.transArray_Emiiter;
        Transform[] trans_array_Emitter = data.bubbleShooter.bubbleEmitter;

        data.f_LaserColliderShootTimer -= Time.deltaTime;

        if (data.f_LaserColliderShootTimer < 0)
        {
            data.bubbleShooter.MakeInvisibleBubbleWithPools(trans_array_Emitter);
            data.f_LaserColliderShootTimer = data.f_LaserColliderShootSpeed;
        }


        //射完一根之後就把發射collider的開關關掉
        //data.b_laserColliderOn = false;
    }

    /// <summary>
    /// 發射雷射，蜘蛛機器人用
    /// </summary>
    /// <param name="data"></param>
    public static void ShootLaser(AI_data data)
    {
        if (data.bool_EnemyHasRemoteAttackProperty == false)
        {
            data.transArray_Emiiter = null;//如果沒有射擊屬性，發射口也給空
            return;
        }

        
        data.f_LaserTimer += Time.deltaTime;


        data.go_ChargeLaser.SetActive(true);
        data.go_SmokeAndSparks.SetActive(true);
        //instance.StartCoroutine( AI_Function.LaserBeamShoot(data));

        
        if (data.f_LaserTimer > 1.2f)
        {
            //    //data.go_LaserCollider.SetActive(true);
            ShootLaserCollider(data);

            ShootLaserHit(data);
            //LaserRender(data);
            //    //data.bubbleShooter.InvisibleBubbleShooting();
            //    return;
        }
        if (data.f_LaserTimer > 1.4f)
        {
            data.go_BeamLaser.SetActive(true);
        }
    }

    /// <summary>
    /// 關掉雷射
    /// </summary>
    /// <param name="data"></param>
    public static void CloseLaser(AI_data data)
    {
        //雷射的計時器歸0(發射雷射的粒子系統用而已，不是跳出狀態)
        data.f_LaserTimer = 0;

        data.go_ChargeLaser.SetActive(false);
        data.go_BeamLaser.SetActive(false);
        data.go_SmokeAndSparks.SetActive(false);
        data.go_EndLaser.SetActive(true);
        //data.go_LaserCollider.SetActive(false);
    }

    public static void ShootLaserHit(AI_data data)
    {
        RaycastHit hit;
        float _laserHitEffectTimer = 0;    
        if (Physics.Raycast(data.go_spiderTurretEmitter.transform.position, data.go_spiderTurretEmitter.transform.forward, out hit, 30f))
        {
            _laserHitEffectTimer += Time.deltaTime;
            if (hit.transform.gameObject.tag=="Controller" )
            {
                data.laserHitEffectPool.ReUse(hit.point, Quaternion.identity);                
            }
            else if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Default"))
            {                
                if (_laserHitEffectTimer > data.f_laserHitGroundMakeEffectTime)
                {
                    data.laserHitGroundEffectPool.ReUse(hit.point + new Vector3(Random.Range(0.0f, 0.2f), 0.1f, 0.5f), Quaternion.identity);
                    data.f_laserHitGroundMakeEffectTime = 0;
                }
            }
        }
    }

    public static void LaserRender(AI_data data)
    {
        RaycastHit hit;
        data.r_laserSpiderLaser=new Ray(data.go_spiderTurretEmitter.transform.position, data.go_spiderTurretEmitter.transform.forward);
        data.lr_laserSpiderLaser.SetPosition(0, data.go_spiderTurretEmitter.transform.position);
        if (Physics.Raycast(data.r_laserSpiderLaser, out hit, 30f))
        {
            if (hit.collider.gameObject.tag=="Controller")
            {
                data.lr_laserSpiderLaser.SetPosition(1, hit.point);
            }
            else
            {
                data.lr_laserSpiderLaser.SetPosition(1, data.go_spiderTurretEmitter.transform.position+data.go_spiderTurretEmitter.transform.forward * 30f);
            }
        }
        
    }

    /// <summary>
    /// 小兵用的Idle
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static bool Idle(AI_data data)
    {
        //data.anim.SetBool("chase", false);
        data.anim.SetTrigger("UndamageTri");
        data.anim.SetBool("take damage", false);
        return true;//回傳的True 或 False 決定了在其他函示裡一 開始的職
    }

    /// <summary>
    /// 怪物受傷，目前直接在OnTriggerEnter裡面執行，所以沒有用到，另外Function裡面也沒東西
    /// </summary>
    /// <param name="data"></param>
    public static void TakeDamage(AI_data data) //受傷
    {

        if (data.takeDamage == true)
        {


            //data.anim.SetTrigger("take damageTri");


        }

        if (data.takeDamage == false)
        {
            // data.anim.SetBool("take damage", false);
            //data.anim.SetTrigger("UndamageTri");
        }
    }

    /// <summary>
    /// 計算與主角間的距離，回傳Float
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static float Distance(AI_data data)
    {
        GameObject controller = data.controller;
        GameObject enemy = data.enemy;

        float distance = Vector3.Distance(enemy.transform.position, controller.transform.position);

        return distance;
    }

    /// <summary>
    /// 檢查是否在慢速區，目前用於小兵的Chase裡面
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static bool SlowDown(AI_data data) //檢查是否在慢速區內
    {
        bool insight;
        if (Distance(data) < data.float_SlowDownDistance)
        {
            insight = true;
            print(data.enemy.name + "SlowDown :" + insight);
        }
        return true;
    }

    /// <summary>
    /// 檢查是否進入攻擊範圍，目前用於小兵的狀態遷移判斷
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static bool InAttackRange(AI_data data) //檢查是否在攻擊區內
    {

        print("InAttackRange");
        if (Distance(data) < data.float_AttackRange)
        {
            //data.Chease_speed = 0f;
            data.anim.SetBool("chase", false);
            data.bool_InAttackRange = true;
            data.isChase = false;

        }
        else if (Distance(data) < data.float_AttackRange && data.bool_InAttackRange == true)
        {
            data.float_nextChase += Time.deltaTime;
            print(" 超出攻擊範圍");
            data.anim.SetBool("chase", false);
            data.Chase_speed = 0f;
            if (data.float_nextChase > data.float_chaseRate && Distance(data) < data.float_AttackRange)   //會在主角離開攻擊範圍後數秒再次追擊
            {
                data.anim.SetBool("chase", true);
                data.isChase = true;
                data.Chase_speed = 0.1f;


                data.bool_InAttackRange = false;

                data.float_nextChase = 0;
            }

            //bool insight;
            //if (Distance(data) < data.float_AttackRange)
            //{
            //    insight = true;
            //    data.anim.SetBool("chase", false);
            //    print(data.enemy.name + "InAttackRange :" + insight);
            //}

        }
        //else
        //{
        //    data.boo1_InAttackRange = false;
        //}
        return true;
    }

    /// <summary>
    /// 後退的動作，目前用於Back狀態
    /// </summary>
    /// <param name="data"></param>
    public static void ToBack(AI_data data)
    {
        data.isChase = false;
        GameObject controller = data.controller;
        GameObject enemy = data.enemy;
        float distance = Vector3.Distance(data.enemy.transform.position, data.controller.transform.position);
        data.dirToPlayer = new Vector3((controller.transform.position.x - enemy.transform.position.x), 0.0f, (controller.transform.position.z - enemy.transform.position.z)).normalized;
        //data.backWaitTimer += Time.deltaTime;
        //if (InBackRange(data))//再後退範圍內
        //{
        //data.Chease_speed = -0.05f;

        //data.anim.SetBool("Back", true);                            
        //if (data.backTime > data.backRate)
        //{
        //    //data.backSpeed = -0.1f;
        //    data.anim.SetBool("Back", true);
        //    data.isChase = false;
        //    data.bool_InAttackRange = false;
        //}
        //else
        //{                    
        //    Idle(data);
        //    UnBack(data);
        //    data.backTime = 0;
        //}           
        Quaternion lookRotation = Quaternion.LookRotation(data.dirToPlayer, Vector3.up);
        enemy.transform.rotation = Quaternion.Slerp(enemy.transform.rotation, lookRotation, Time.deltaTime * data.Rotationspeed);
        data.movementVec = enemy.transform.forward * data.backSpeed * Time.deltaTime;

        //data.movementVec = enemy.transform.forward * -0.1f;
        data.anim.SetBool("Back", true);
        data.rb.MovePosition(data.rb.transform.position + data.movementVec * 1f);
        data.isBack = true;

        //}
        // if (data.backTime < data.backRate)  //  在進入後退的那段時間
        //{
        //    //Quaternion lookRotation = Quaternion.LookRotation(data.dirToPlayer, Vector3.up);
        //    //enemy.transform.rotation = Quaternion.Slerp(enemy.transform.rotation, lookRotation, Time.deltaTime * data.Rotationspeed);
        //    ////data.movementVec = enemy.transform.forward * data.backSpeed;

        //    //data.movementVec = enemy.transform.forward * -0.1f;
        //    //data.anim.SetBool("Back", false);
        //    //data.rb.MovePosition(data.rb.transform.position + data.movementVec * 1f);
        //    //data.isBack = false;
        //    UnBack(data);
        //}

        //if (data.inBackRange == false)
        //{
        //    data.anim.SetBool("Back", false);
        //    data.backTime = 0;
        //}
    }

    /// <summary>
    /// 追擊的動作，目前用於Chase狀態
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static bool ToChase(AI_data data)
    {
        GameObject enemy = data.enemy;
        RaycastHit hit;
        Vector3 v = data.dirToPlayer;
        GameObject controller = data.controller;
        data.dirToPlayer = new Vector3((controller.transform.position.x - enemy.transform.position.x), 0.0f, (controller.transform.position.z - enemy.transform.position.z)).normalized;

        float angle = Mathf.Atan2(data.dirToPlayer.x, data.dirToPlayer.z);
        float distance = Vector3.Distance(enemy.transform.position, controller.transform.position);
        if (!Dead(data))
        {
            if (distance > data.float_checkInSight)
            {
                data.anim.SetBool("chase", false);
                data.isChase = false;
                return false;
            }

            if (distance < data.float_checkInSight)
            {

                if (Physics.Raycast(enemy.transform.position, enemy.transform.forward, out hit, data.rayDistance) && (hit.collider.gameObject.tag != "Controller" + "Scene_Street "))//撞到牆要迴避
                {

                    Debug.DrawLine(enemy.transform.position, hit.point, Color.red);
                    print(enemy.name + "碰撞物:" + hit.collider.gameObject.name);
                    data.dirToPlayer += hit.normal * 30.0f;

                }
                else if (Physics.Raycast(enemy.transform.position, enemy.transform.forward, out hit, data.rayDistance) && (hit.collider.gameObject.tag == "Enemy"))
                {
                    Debug.Log("敵人的前方有敵人 " + hit.collider.gameObject.name);
                    data.dirToPlayer = hit.normal * 10.0f;
                }
                else
                {
                    // dirToPlayer = new Vector3((target.transform.position.x - this.transform.position.x), 0.0f, (target.transform.position.z - this.transform.position.z)).normalized;
                    Debug.DrawRay(enemy.transform.position, enemy.transform.forward * data.rayDistance, Color.yellow);
                    //dirToPlayer = dirToPlayer;
                }

                Quaternion lookRotation = Quaternion.LookRotation(data.dirToPlayer, Vector3.up);
                enemy.transform.rotation = Quaternion.Slerp(enemy.transform.rotation, lookRotation, Time.deltaTime * data.Rotationspeed);
                data.movementVec = enemy.transform.forward * data.Chase_speed;
                //////
                float speed = data.Chase_speed;
                data.anim.SetBool("chase", true);
                data.rb.MovePosition(data.rb.transform.position + data.movementVec * 1f);
                data.isChase = true;
            }

            if (distance > data.float_AttackRange && distance <= data.float_SlowDownDistance)
            {

                Quaternion lookRotation = Quaternion.LookRotation(data.dirToPlayer, Vector3.up);
                enemy.transform.rotation = Quaternion.Slerp(enemy.transform.rotation, lookRotation, Time.deltaTime * data.Rotationspeed);
                data.movementVec = enemy.transform.forward * data.Chase_speed;


            }

            if (distance < data.float_AttackRange)  // 當距離小於攻擊範圍 速度為0  開始攻擊 並且 當離開攻擊範圍後  開始計時 並且兩秒後 才開始追擊
            {
                data.Chase_speed = 0f;
                data.anim.SetBool("chase", false);
                data.bool_InAttackRange = true;
                data.isChase = false;

            }
            else if (distance > data.float_AttackRange && data.bool_InAttackRange == true)
            {
                data.float_nextChase += Time.deltaTime;
                print(" 超出攻擊範圍");
                data.anim.SetBool("chase", false);
                data.Chase_speed = 0f;
                if (data.float_nextChase > data.float_chaseRate && distance > data.float_AttackRange)   //會在主角離開攻擊範圍後數秒再次追擊
                {
                    data.anim.SetBool("chase", true);
                    data.isChase = true;
                    data.Chase_speed = 0.1f;


                    data.bool_InAttackRange = false;

                    data.float_nextChase = 0;
                }


            }

        }

        return true;

    }

    /// <summary>
    /// 凱爾狂暴狀態時的移動，目前用於壞人凱爾專用的狂暴狀態
    /// </summary>
    /// <param name="data"></param>
    public static void KyleCrazyMove(AI_data data)
    {
        data.dirToPlayer = data.controller.transform.position - data.enemy.transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(data.dirToPlayer, Vector3.up);
        data.enemy.transform.rotation = Quaternion.Slerp(data.enemy.transform.rotation, lookRotation, Time.deltaTime * data.Rotationspeed);
        data.movementVec = data.dirToPlayer * data.Chase_speed;

        data.rb.MovePosition(data.rb.transform.position + data.movementVec * 0.6f);
    }

    /// <summary>
    /// 凱爾被震倒
    /// </summary>
    /// <param name="data"></param>
    public static void FallingBackDown(AI_data data)
    {

        data.anim.SetTrigger("falling");
        data.CloseEnemyCol(); //被擊倒時關閉Col
        data.isFallingBackDown = true;

    }

    /// <summary>
    /// 凱爾站起來
    /// </summary>
    /// <param name="data"></param>
    public static void StandUp(AI_data data)
    {
        if (data.isStandUp == true)
        {
            data.anim.SetTrigger("DownUp");
        }
        data.isStandUp = false;
    }

    /// <summary>
    /// 近攻攻擊，內建計時器，目前用於凱爾、長腳機器人的攻擊狀態
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static bool MelleAttack(AI_data data)
    {

        Animator animator = data.anim;
        bool isAttack = false;


        //data.enemyTarget.enemyDamage_Melle = data.int_MelleAttackD_Damage; //近戰攻擊力
        Transform thisEnemyTransfrom = data.enemy.transform;//這隻怪獸的位置一開始的Trasform，先存起來
        Vector3 thisEnemyPosition = data.enemy.transform.position;//這怪獸的位置，先存起來


        Vector3 controller = data.controller.transform.position;// 找到主角
        bool startAttack = false;
        float attackRange = data.float_AttackRange;
        float attackRate = data.float_MelleAttackRate;
        float disForBoth = Vector3.Distance(controller, thisEnemyPosition);
        float nextAttack = data.float_NextMelleAttack;
        string[] attackAnim = data.MelleAttackAnim;
        string animation = null;
        if (data.bool_EnemyHasMelleAttackProperty == true)//如果是近戰
        {

            int attackAmin = UnityEngine.Random.Range(0, data.MelleAttackAnim.Length);
            if (CheckControllerInSight(data, ref isAttack) && data.takeDamage == false && data.isChase == false)
            {

                if (NextTimeAttack(data, ref startAttack) && data.isDead == false)
                {
                    animation = data.MelleAttackAnim[attackAmin];
                    //animator.SetBool("attack_01", isAttack);
                    isAttack = true;
                    data.anim.SetTrigger("UndamageTri");
                    print(data.enemy.name + "開始攻擊 :" + isAttack);
                    animator.CrossFade(animation, 0.0001f);
                }
                else
                {
                    animator.SetBool("attack_01", false);
                    data.anim.SetTrigger("UndamageTri");
                }

                // animation = data.MelleAttackAnim[attackAmin]; // 隨機攻擊動畫  但因為同時讀條 而讓動畫卡住



                //animator.CrossFade(animation, 0.0001f);
            }
            //else 
            //{

            //    animator.SetBool("attack_01", false);
            //    return;
            //}

        }

        return true;

    }

    /// <summary>
    /// 護衛機的傷害，目前用於FightSystem
    /// </summary>
    /// <param name="data"></param>
    /// <param name="fightSystem"></param>
    public static void MachineDamage(AI_data data, FightSystem fightSystem) //護衛機的傷害
    {
        if (data.machineDamage == true)
        {

            //print("敵人當前血量 : " + data.currentHp);
            //data.anim.SetTrigger("take damageTri");
        }
        if (data.machineDamage == false)
        {
            data.anim.SetBool("take damage", false);
        }


    }

    /// <summary>
    /// 小兵死亡，開死亡動畫，關Collider而已
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static bool Dead(AI_data data)
    {

        if (data.currentHp <= 0)
        {

            data.currentHp = 0;
            print("dead!");
            data.anim.SetBool("dead", true);
            //Destroy(data.gameObject, 20f);
            data.rb.constraints = RigidbodyConstraints.FreezeAll;
            data.thisEnemyCollider.enabled = false;
            data.isDead = true;
            return data.isDead;
            //data.enabled = false;
        }

        return false;

    }

    /// <summary>
    /// 小兵摧毀屍體，括號裡給個float決定摧毀的時間，只能拿來摧毀自己的屍體
    /// </summary>
    /// <param name="data"></param>
    /// <param name="time"></param>
    public static void DestroyDeadBody(AI_data data, float time)
    {
        Destroy(data.enemy, time);
    }

    /// <summary>
    /// 攻擊間隔，目前使用在MelleAttack的動作裡
    /// </summary>
    /// <param name="data"></param>
    /// <param name="_startAttack"></param>
    /// <returns></returns>
    public static bool NextTimeAttack(AI_data data, ref bool _startAttack) //攻擊間隔
    {

        data.float_NextMelleAttack += Time.deltaTime;
        print("開始倒數 : " + data.float_FirstAttackCountDown);
        if (data.float_NextMelleAttack > data.float_MelleAttackRate) //下次攻擊倒數 
        {
            data.float_NextMelleAttack = 0;
            return true;
        }
        else
        {
            _startAttack = false;
            return false;
        }

    }

    /// <summary>
    /// 炮台瞄準，用在蜘蛛機器人身上
    /// </summary>
    /// <param name="data"></param>
    public static void SpiderTurrentRotation(AI_data data)
    {
        if (data.go_spiderTurret == null || data.go_spiderTurretEmitter == null)
        {
            Debug.LogError("請把蜘蛛機器人身上的砲台拉到AI_data的「各種射擊設定」裡的go_spiderTurrent那邊去\n" +
                "還有發射口(炮管)上有一個空物件要拉到AI_data 的 go_spiderTurrentEmitter那邊");
        }
        //加了下面這一行會讓瞄準度UP但是因為一秒更新60次，所以蜘蛛砲台會一直抖
        //data.v_spiderAimingAngle = new Vector3(Random.Range(0.1f ,1.15f), 0f, Random.Range( 0.1f, 1.15f));
        //Quaternion lookRotation = Quaternion.LookRotation(new Vector3(data.controller.transform.position.x - data.go_spiderTurretEmitter.transform.position.x,
        //    0.3f, data.controller.transform.position.z - data.go_spiderTurret.transform.position.z) + data.v_spiderAimingAngle, Vector3.up);

        Quaternion lookRotation = Quaternion.LookRotation((data.controller.transform.position - data.go_spiderTurretEmitter.transform.position) + data.v_spiderAimingAngle, Vector3.up);
        data.go_spiderTurret.transform.rotation = Quaternion.Slerp(data.go_spiderTurret.transform.rotation, lookRotation, data.f_spiderTurretRotationSpeed * Time.deltaTime);
    }

    #endregion

    #region 大魔王用的function

    public static float LastBossTimer(AI_data data)
    {
        data.f_LBTimer += Time.deltaTime;
        return data.f_LBTimer;
    }

    public static void LastBossRotate(AI_data data)//大魔王轉向
    {
        //轉向
        if (data.b_LBRotating == false) { return; }

        data.dirToPlayer = new Vector3(data.controller.transform.position.x - data.enemy.transform.position.x, 0.0f, data.controller.transform.position.z - data.enemy.transform.position.z);
        Quaternion lookRotation = Quaternion.LookRotation(data.dirToPlayer, Vector3.up);




        data.enemy.transform.rotation = Quaternion.Slerp(data.enemy.transform.rotation.normalized, lookRotation, data.f_LBRotationSpeed);
    }

    /// <summary>
    /// float attackRange ，是走到離主角多少距離的地方
    /// </summary>
    public static void LastBossMoveForward(AI_data data, float attackRange)//大魔王移動
    {
        if (data.b_LBMoving == false) { return; }
        GameObject enemy = data.enemy;
        GameObject controller = data.controller;
        //下面是算距離的向量
        data.dirToPlayer = new Vector3((controller.transform.position.x - enemy.transform.position.x), 0.0f, (controller.transform.position.z - enemy.transform.position.z));

        data.v_LBVectorChase = enemy.transform.forward * data.f_LBMoveSpeed;//有乘上速度的向量

        if (Vector3.Magnitude(data.dirToPlayer) > attackRange)
        {
            data.rb_LBrb.MovePosition(enemy.transform.position + data.v_LBVectorChase * 0.8f);//關鍵的移動的這一行
        }
        else
        {

        }


        // var LbMove = Vector3.Lerp(enemy.transform.position, enemy.transform.forward, 0.5f);
    }

    public static void LastBossMoveBackward(AI_data data)//大魔王移動
    {
        if (data.b_LBMoving == false) { return; }
        GameObject enemy = data.enemy;
        GameObject controller = data.controller;
        //下面是算距離的向量
        data.dirToPlayer = new Vector3((controller.transform.position.x - enemy.transform.position.x), 0.0f, (controller.transform.position.z - enemy.transform.position.z));
        data.v_LBVectorChase = enemy.transform.forward * data.f_LBMoveSpeed;//有乘上速度的向量
        if (Vector3.Magnitude(data.dirToPlayer) < 10)
        {
            data.rb_LBrb.MovePosition(enemy.transform.position - data.v_LBVectorChase * 0.8f);
            //data.rb_LBrb.velocity = new Vector3();
        }
        else
        {


        }
        // var LbMove = Vector3.Lerp(enemy.transform.position, enemy.transform.forward, 0.5f);
    }

    public static void LastBossAvoidObstacle(AI_data data)//大魔王避開牆，抄少彥的
    {
        GameObject enemy = data.enemy;
        RaycastHit hit;

        if (Physics.Raycast(enemy.transform.position, enemy.transform.forward, out hit, data.rayDistance) && (hit.collider.gameObject.tag != "Controller" + "Scene_Street "))//撞到牆要迴避
        {

            Debug.DrawLine(enemy.transform.position, hit.point, Color.red);
            print(enemy.name + "碰撞物:" + hit.collider.gameObject.name);
            data.dirToPlayer += hit.normal * 30.0f;

        }
        else
        {
            // dirToPlayer = new Vector3((target.transform.position.x - this.transform.position.x), 0.0f, (target.transform.position.z - this.transform.position.z)).normalized;
            Debug.DrawRay(enemy.transform.position, enemy.transform.forward * data.rayDistance, Color.yellow);
            //dirToPlayer = dirToPlayer;
        }

    }


    public static void LastBossShoot(AI_data data, Transform[] emitters)
    {
        //是否具有遠距攻擊屬性，沒的話就掰
        if (data.bool_EnemyHasRemoteAttackProperty == false)
        {
            data.transArray_Emiiter = null;//如果沒有射擊屬性，發射口也給空
            return;
        }

        //把AI_data的泡泡射擊速度 傳到 BubbleShoot_3 裡，再把它存起來
        data.bubbleShooter.resetShootingTime = data.float_BubbleShootSpeed;
        float f_BubbleShootSpeed = data.bubbleShooter.resetShootingTime;
        //同上，第一次射擊前的延遲時間
        data.bubbleShooter.firstShootingCountDown = data.float_FirstShootDelayTime;
        float f_FirstShootDelayTime = data.bubbleShooter.firstShootingCountDown;
        //同上，一波泡泡攻擊時間
        data.bubbleShooter.totalWaveShootingTime = data.float_WaveShootTotalTime;
        float f_WaveShootTotalTime = data.bubbleShooter.totalWaveShootingTime;
        //同上，整波的射完之後的延遲時間
        data.bubbleShooter.rechargeTime = data.float_WaveShootOverDelayTime;
        float f_WaveShootOverDelayTime = data.bubbleShooter.rechargeTime;
        //同上，泡泡的發射口(注意是一個陣列)
        data.bubbleShooter.bubbleEmitter = data.transArray_Emiiter;
        emitters = data.bubbleShooter.bubbleEmitter;



        //Debug.Log("AIFunction");

        data.bubbleShooter.BubbleShooting();
    }

    public static void LastBossCheckOnGround(AI_data data)
    {
        RaycastHit hit;
        if (Physics.Raycast(data.enemy.transform.position, -data.enemy.transform.up, out hit, 5.0f))
        {
            data.enemy.transform.position = Vector3.Slerp(data.enemy.transform.position, new Vector3(data.enemy.transform.position.x, hit.point.y + 0.08f, data.enemy.transform.position.z), 0.5f);
        }
    }

    public static void LastBossShootEmitterRotate(AI_data data)
    {
        if (data.b_LBEmitterRotate == false) { return; }
        data.bubbleShooter.BubbleMakerRotate(data.go_LBEmitterHolder, 0, 0, 30f);

    }

    public static void LastBossJumpMoveForward(AI_data data)
    {
        data.enemy.transform.position = Vector3.Slerp(data.enemy.transform.position, data.enemy.transform.position + data.enemy.transform.forward * 0.8f, 1.0f);
    }
    #endregion

    #region NiceKyle專用Function

    public static void NiceKyleRotate(AI_data data)//好凱爾轉向
    {
        //轉向
        if (data.b_NKRotate == false) { return; }

        data.dirToPlayer = new Vector3(data.controller.transform.position.x - data.enemy.transform.position.x, 0.0f, data.controller.transform.position.z - data.enemy.transform.position.z);
        Quaternion lookRotation = Quaternion.LookRotation(data.dirToPlayer, Vector3.up);

        data.enemy.transform.rotation = Quaternion.Slerp(data.enemy.transform.rotation.normalized, lookRotation, data.f_NKRotateSpeed);
    }

    public static void NiceKyleRotateToOtherAngle(AI_data data, Quaternion angle)
    {
        data.enemy.transform.rotation = Quaternion.Slerp(data.enemy.transform.rotation.normalized, angle, data.f_NKRotateSpeed);
    }
    public static void NiceKyleCalculateAngleToTarget(AI_data data, GameObject targetPosotion)
    {

    }

    public static void NiceKyleMove(AI_data data, GameObject targetPos)//好凱爾移動
    {
        if (data.b_NKMove == false) { return; }
        GameObject enemy = data.enemy;

        //下面是算距離的向量
        data.v_NKdirToTarget = new Vector3((targetPos.transform.position.x - enemy.transform.position.x), 0.0f, (targetPos.transform.position.z - enemy.transform.position.z));

        data.v_NKmoveVector = enemy.transform.forward * data.f_NKMoveSpeed;//有乘上速度的向量

        if (Vector3.Magnitude(data.v_NKdirToTarget) > 0.1f)
        {
            data.rb.MovePosition(enemy.transform.position + data.v_NKmoveVector * 0.8f);//關鍵的移動的這一行
        }
        else
        {

        }


        // var LbMove = Vector3.Lerp(enemy.transform.position, enemy.transform.forward, 0.5f);
    }

    public static GameObject CheckOtherEnemyInSight(AI_data data)
    {
        Vector3 niceKylePos = data.enemy.transform.position;

        return null;
    }

    public static bool CheckAllEnemiesDistanceIsInNiceKyleAttackRange(AI_data data)
    {
        
        int enemiesIndex = -1;
        for (int i = 0; i < data.otherEnemies.Length; i++)
        {
            data.f_DistanceOfAllEnemiesToNiceKyle = Vector3.Distance(data.otherEnemies[i].transform.position, data.enemy.transform.position);
            if (data.f_DistanceOfAllEnemiesToNiceKyle < data.f_NKAttackRange)
            {
                data.b_enemiesInNiceKyleAttackRange = true;
                enemiesIndex = i;
                data.f_DistanceOfNearstEnemyToNiceKyle = data.f_DistanceOfAllEnemiesToNiceKyle;
                data.go_nearestEnemyToNiceKyle = data.otherEnemies[i];
                return true;
            }
            
        }
        
        data.b_enemiesInNiceKyleAttackRange =false;
        return false;
    }

    public static GameObject CheckNearestEnemyToNiceKyle(AI_data data)
    {
        int enemiesIndex =-1;
        for (int i = 0; i < data.otherEnemies.Length; i++)
        {
            data.f_DistanceOfAllEnemiesToNiceKyle = Vector3.Distance(data.otherEnemies[i].transform.position, data.enemy.transform.position);
            if (data.f_DistanceOfAllEnemiesToNiceKyle < data.f_NKAttackRange)
            {
                enemiesIndex = i;
                data.go_nearestEnemyToNiceKyle = data.otherEnemies[i];
                data.f_DistanceOfNearstEnemyToNiceKyle = Vector3.Distance(data.go_nearestEnemyToNiceKyle.transform.position, data.enemy.transform.position);
            }
        }
        data.go_nearestEnemyToNiceKyle = null;

        return data.go_nearestEnemyToNiceKyle;
    }
    #endregion

    #region 壞人凱爾找到好人凱爾
    public static bool CheckAllNiceKyleDistanceIsInBadKyleAttackRange(AI_data data)
    {
        int enemiesIndex = -1;
        for (int i = 0; i < data.niceKyles.Length; i++)
        {
            data.f_DistanceOfNiceKyleToBadKyle = Vector3.Distance(data.niceKyles[i].transform.position, data.enemy.transform.position);
            if (data.f_DistanceOfNiceKyleToBadKyle < data.float_checkInSight)
            {
                data.b_badKyleFindNiceKyle = true;
                enemiesIndex = i;
                data.f_DistanceOfNearestNiceKyleToBadKyle = data.f_DistanceOfNiceKyleToBadKyle;
                data.go_nearestNiceKyleToBadKyle = data.niceKyles[i];
                return true;
            }

        }

        data.b_badKyleFindNiceKyle = false;
        return false;
    }

    /// <summary>
    /// 壞人凱爾追擊好人凱爾的位移
    /// </summary>
    /// <param name="data"></param>
    /// <param name="target"></param>
    public static void BadKyleChaseNiceKyle(AI_data data, GameObject target)
    {
        data.dirToPlayer = target.transform.position - data.enemy.transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(data.dirToPlayer, Vector3.up);
        data.enemy.transform.rotation = Quaternion.Slerp(data.enemy.transform.rotation, lookRotation, Time.deltaTime * data.Rotationspeed);
        data.movementVec = data.dirToPlayer.normalized * data.Chase_speed;

        data.rb.MovePosition(data.rb.transform.position + data.movementVec * 0.6f);
    }
    #endregion

    #region 凱爾牆
    public static void KyleWallRotate(AI_data data, GameObject target)
    {
        data.dirToPlayer = target.transform.position - data.enemy.transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(data.dirToPlayer, Vector3.up);
        data.enemy.transform.rotation = Quaternion.Slerp(data.enemy.transform.rotation, lookRotation, Time.deltaTime * data.Rotationspeed);
    }
    #endregion
}








